<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta name="description" content="" />
    <meta name="author" content="fedops" />
    <meta name="generator" content="Pelican (VoidyBootstrap theme)" />

    <title>Installing and Using pandoc on Fedora/Centos/Rocky - fedops blog</title>

   
        <link rel="stylesheet"
              href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
              integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u"
              crossorigin="anonymous" />

      <link rel="stylesheet"
            href="https://use.fontawesome.com/releases/v5.0.13/css/all.css"
            integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp"
            crossorigin="anonymous"
      />




      <link rel="stylesheet" href="/theme/css/pygment.css" />
      <link rel="stylesheet" href="/theme/css/voidybootstrap.css" />

    <link rel="shortcut icon" href="/favicon.ico" />
  </head>

  <body>
   
    <nav class="navbar navbar-default">
      <div class="container">
	   <div class="navbar-header">
		<button type="button" class="navbar-toggle" 
				data-toggle="collapse" data-target="#main-navbar-collapse">
		  <span class="sr-only">Toggle navigation</span>
		  <span class="icon-bar"></span>
		  <span class="icon-bar"></span>
		  <span class="icon-bar"></span>
		</button>
		<a class="navbar-brand" href="/" rel="home">
          <i class="fas fa-home fa-fw fa-lg"> </i> </a>
       </div>

      <div class="collapse navbar-collapse" id="main-navbar-collapse">
        <ul class="nav navbar-nav">
              <li>
                <a href="/pages/about-this-blog.html">About this Blog</a>
              </li>
            <li class="divider"></li>
            <li class="">
              <a href="/archives.html">Archives</a>
            </li>
          <li class="divider"></li>
        </ul> <!-- /nav -->
      </div> <!-- /navbar-collapse -->
	  </div> <!-- /container -->
    </nav> <!-- /navbar -->

	<div class="jumbotron" id="overview">
	  <div class="container">
		<h1><a href="/">fedops blog</a></h1>
		<p class="lead">Privacy in Computing</p>
	  </div>
	</div>

    <div class="container" id="main-container">
      <div class="row">
        <div class="col-md-9" id="content">
<article itemscope="itemscope" itemtype="http://schema.org/BlogPosting">
  <header class="article-header">
<abbr class="article-header-date">
  Mon 28 March 2022
</abbr> <h1>
  <a href="/installing-and-using-pandoc-on-fedoracentosrocky.html" rel="bookmark"
     title="Permalink to Installing and Using pandoc on Fedora/Centos/Rocky">
    Installing and Using pandoc on Fedora/Centos/Rocky
  </a>
</h1><div class="article-header-info">
  <p>
      Posted by <a href="/author/fedops.html">fedops</a>
    in 
    <a href="/category/howto.html">
      Howto</a>
    &nbsp;&nbsp;
  </p>
</div> <!-- /.article-header-info -->  </header>
  <div class="content-body" itemprop="text articleBody">
	<p>Documentation-As-Code is a much better approach than your WYSIWYG editor of old.</p>
<p>The general consensus regarding how to create and maintain documentation has
definitely changed over the past few years. People had come to regard WYSIWYG
text processors as the gold standard. Effortlessly arrange text and images, see
what you are getting in real time, and everything is just a click away. Besides
the big commercial packages there is also a sprawling landscape of free
software, such as LibreOffice that many people happily use.</p>
<div class="toc"><span class="toctitle">Table of Contents</span><ul>
<li><a href="#alternatives">Alternatives</a><ul>
<li><a href="#restmarkdown">Rest/Markdown</a></li>
</ul>
</li>
<li><a href="#the-pandoc-system">The pandoc system</a><ul>
<li><a href="#pandoc">pandoc</a></li>
<li><a href="#pandoc-crossref">pandoc-crossref</a></li>
<li><a href="#rsvg-convert">rsvg-convert</a></li>
<li><a href="#texlive">texlive</a></li>
<li><a href="#eisvogel-template">Eisvogel template</a></li>
<li><a href="#setup-environment">Setup environment</a></li>
<li><a href="#test-an-example">Test an example</a></li>
</ul>
</li>
<li><a href="#documents-as-code">"Documents as Code"</a></li>
</ul>
</div>
<p>However, the disadvantages have become ever more obvious:</p>
<ul>
<li>most word processors use binary formats to store the documents, usually in one
  big file or a "camouflaged" zip archive containing multiple files.</li>
<li>said files are usually in proprietary formats and not only not easily
  translatable to other software packages, but also fast-changing within one
  software suite.</li>
<li>meaningful version control is nigh-on impossible.</li>
<li>comparing changes between versions is usually only possible with
  software-specific tools, or not at all.</li>
<li>converting or processing files for different output formats is a mostly
  manual process.</li>
<li>writers tend to spend way too much time obsessing over formatting minutiae or
  fighting the software instead of concentrating on the content of their
  document.</li>
<li>most formatting is haphazard and "for show" only, usually using formatting
  styles which may or may not carry across documents and software versions.</li>
</ul>
<h1 id="alternatives">Alternatives</h1>
<p>Better methods have existed for decades, such as the roff/nroff/groff
typesetting systems for Unix, SGML, or (La)TeX for every system under the sun.
Usually there is a fairly steep learning curve involved, which means their use
was limited to special situations or was only picked up by people writing a lot
of intricate documents, such as researchers or technical writers (the
profession).</p>
<h2 id="restmarkdown">Rest/Markdown</h2>
<p>Lately several low-barrier-to-entry documentation formats have come onto the
scene. They have been adopted especially by developers, such as reStructured
Text and Markdown. While nowhere near as flexible as for example TeX, they
follow the 80/20 principle. 20% of effort nets 80% of the gain, there are
workarounds for advanced use cases, and a lot of the burden of writing
documentation is removed. Which is a crucial first step in actually having
documentation written!</p>
<p>What's more, being plain-text based these documents can be:</p>
<ul>
<li>easily version-controlled</li>
<li>collaboratively edited with standard conflict-resolving merge strategies</li>
<li>are light-weight, completely open, and require no special editing tools</li>
<li>can be read as-is in source form without losing any content</li>
<li>can be automatically converted into HTML web pages and other document formats</li>
</ul>
<p>The apparent sparseness of markup also means that a special WYSIWYG editor isn't required
for many use cases - what is in your editor is fine to read as-is. Also,
especially Markdown has become the darling of Git forges and as such can be
readily rendered and edited in web interfaces.</p>
<h1 id="the-pandoc-system">The pandoc system</h1>
<p>One of THE best tools to work with many of these formats is
<a href="https://pandoc.org/">pandoc</a>. It can process various input formats, among them
Markdown in various flavors and reStructured Text. On the output side it again
supports a slew of formats, among them HTML and PDF output - the latter
by way of LaTeX typesetting, guaranteeing the highest quality. There exist many
extensions to pandoc itself, and of course one can use any of the zillion of
add-on packages for LaTeX or develop new ones, making this a fairly easy
step up from or indeed a side-by-side to hand-editing LaTeX.</p>
<p>One of the downsides is that the complete pandoc system tends to be somewhat
involved to set up. There isn't a one-stop shop solution with up to date
packages available for RPM-based systems. So here's what I did with the hope of
it being useful to you as well.</p>
<p>This is current as of 28-Mar-2022 and was done on a Rocky Linux 8.5 system.</p>
<h2 id="pandoc">pandoc</h2>
<p>This is the actual pandoc software itself. I found it easiest to download the
correct <a href="https://pandoc.org/installing.html">pandoc binary for your architecture</a>. If you
prefer to build it yourself then documentation is available there.</p>
<p>Once you have it downloaded, unpack the binary into <code>/usr/local/pandoc/&lt;version&gt;</code> and
create a symlink (using <code>ln -s</code>) from the binary into <code>/usr/local/pandoc</code> so the
shell will find it in your $PATH. Using this approach enables you to maintain
multiple pandoc releases on the same system. If something goes wrong with a new
version you just need to adjust the symlink to point to the older one and you're
back in business until you get the problem fixed.</p>
<h2 id="pandoc-crossref">pandoc-crossref</h2>
<p>The is the cross-referencer for pandoc, a very useful extension. Download the
suitable tar archive from the <a href="https://github.com/lierdakil/pandoc-crossref">releases page at github</a>
and unpack it somewhere.</p>
<p>Move the executable into <code>/usr/local/bin</code> so it can be found in the $PATH by pandoc and the man page into
<code>/usr/local/share/man/man1</code> so it's in your MANPATH.</p>
<h2 id="rsvg-convert">rsvg-convert</h2>
<p>This is the SVG converter which will enable you to use vector images in your
documents. Install using <code>dnf install librsvg2-tools</code>.</p>
<h2 id="texlive">texlive</h2>
<p>The Rocky/Centos packaged texlive version is very old and misses templates. This
must be installed from scratch following the instructions given on the 
<a href="https://tug.org/texlive/acquire-netinstall.html">TeX User Group's web site</a>.</p>
<p>Start by downloading the <code>install.tar.gz</code> installer archive and unpacking it
somewhere (e.g. your <code>~/Downloads</code> directory; you can delete it again after installation
is complete).</p>
<p>Then, as root create and chown directory <code>/usr/local/texlive</code> to your non-root
user running the installation.</p>
<p>Finally, <code>cd</code> into the unpacked installer and run <code>perl ./install-tl</code>. This will
download thousands of packages and run for quite a while depending on your
machine and Internet connection speeds. When the process is complete you will
find the installation in <code>/usr/local/texlive</code>, owned by you. If you wish you
can run a <code>sudo chown -R bin:bin /usr/local/texlive</code> on the entire directory tree to make it
accessible to all users on your system.</p>
<h2 id="eisvogel-template">Eisvogel template</h2>
<p>A template I have really grown to like is Eisvogel. It can be used to create
professional looking documents in corporate environments.</p>
<p>If you want to install it, download the tar.gz file from the <a href="https://github.com/Wandmalfarbe/pandoc-latex-template">releases page of github</a>.
Unpack it into <code>~/.local/share/pandoc/templates</code> - do a <code>mkdir -p
~/.local/share/pandoc/templates</code> if that doesn't exist yet. If you later find
additional templates you want to use, they all install into that directory.</p>
<h2 id="setup-environment">Setup environment</h2>
<p>For every user that should work with pandoc, add the following to <code>~/.bashrc</code> or
<code>~/.bash_profile</code> (or equivalent startup files if you use another shell):</p>
<pre><code># User specific environment
if ! [[ &quot;$PATH&quot; =~ &quot;$HOME/.local/bin:$HOME/bin:&quot; ]]
then
    PATH=&quot;$HOME/.local/bin:$HOME/bin:$PATH:/usr/local/texlive/2021/bin/x86_64-linux&quot;
fi
export PATH

# for pandoc
MANPATH=${MANPATH}:/usr/local/texlive/2021/texmf-dist/doc/man
INFOPATH=${INFOPATH}:/usr/local/texlive/2021/texmf-dist/doc/info
export MANPATH INFOPATH
</code></pre>
<h2 id="test-an-example">Test an example</h2>
<p>Go into <code>cd ~/.local/share/pandoc/templates/examples/basic-example</code> and run:
<code>pandoc basic-example.md -o basic-example.pdf --from markdown --template
eisvogel --listings</code>.</p>
<p>This should create beautifully formatted output in basic-example.pdf which is
styled with the Eisvogel template.</p>
<h1 id="documents-as-code">"Documents as Code"</h1>
<p>With this out of the way you are now in a position to start writing your
documents in Rest or Markdown. I'd suggest starting a new (sub-)directory with a
useful hierarchy to store them, and to take that under Git control so you always
have backups and version management.</p>
<p>A fantastic software development use case is to keep your documentation as
reStructured Text or Markdown files inside of your source code repo. You would
treat them as regular source files, including tagging for releases. Then add a
pandoc run to your CI flows to ensure that the current version of documentation
is built along with your software artifacts. The exact same documentation would
then be available as an HTML document tree, and as a shipped PDF file. And of
course you have full access to Git tags or commit hashes which you can use in
places such as document headers or front matter.</p>
<p>Note that pandoc can be run on lists of input files, processing them into a
single output file. So I tend to write my documents as single chapter files;
e.g. <code>00-cover.md, 01-scope.md, 02-intro.md, ..., xx-glossary.md</code>. Running <code>pandoc
*.md -o document.pdf</code> automatically assembles them in the correct order. No more
futzing around with splitting and merging documents. Want to add a chapter?
Simply create a new file.</p>
<p>Similarily, it's also a piece of cake to auto-generate some
content. Let's say you want to include a list of error messages as an appendix
chapter. A shell script in your build pipeline can extract that information from
the source code, beautify it with some markup, and write it to a specific file.</p>
<p>The possibilities are endless once you move beyond the point &amp; click way of
creating documentation.</p>
  </div>
  
<div class="article-tag-list">
<span class="label label-default">Tags</span>
	<a href="/tag/markdown.html"><i class="fas fa-tag"></i>markdown</a>&nbsp;
	<a href="/tag/documentation.html"><i class="fas fa-tag"></i>documentation</a>&nbsp;
	<a href="/tag/software.html"><i class="fas fa-tag"></i>software</a>&nbsp;
</div><!-- via neighbor plugin, see: https://github.com/pelican-plugins/neighbors -->
    <hr />
    <p class="content-emphasis">
	<table width="100%">
	    <tr>
			<td align="left">
	        </td>
	        <td align="right">
	        </td>
		</tr>
	</table>
	</p>
</article>
        </div><!-- /content -->

        <div class="col-md-3 sidebar-nav" id="sidebar">

<div class="row">

<div class="col-xs-6 col-md-12">
<h4><i class="fas fa-comment fa-fw fa-lg"></i> Social</h4>
<ul class="list-unstyled social-links">
    <li><a href="https://fosstodon.org/@fedops" target="_blank">
	  <i class="fas fa-comments fa-fw fa-lg" title="Fosstodon"></i>
		Fosstodon
	</a></li>
    <li><a href="https://codeberg.org/fedops" target="_blank">
	  <i class="fas fa-comments fa-fw fa-lg" title="Codeberg"></i>
		Codeberg
	</a></li>
</ul>
</div>

<div class="col-xs-6 col-md-12">
<h4><i class="fas fa-folder fa-fw fa-lg"></i> Categories</h4>
<ul class="list-unstyled category-links">
  <li><a href="/category/cloud.html" >
    <i class="fas fa-folder-open fa-fw fa-lg"></i> Cloud</a></li>
  <li><a href="/category/hardware.html" >
    <i class="fas fa-folder-open fa-fw fa-lg"></i> Hardware</a></li>
  <li><a href="/category/howto.html" >
    <i class="fas fa-folder-open fa-fw fa-lg"></i> Howto</a></li>
  <li><a href="/category/infomanagement.html" >
    <i class="fas fa-folder-open fa-fw fa-lg"></i> InfoManagement</a></li>
  <li><a href="/category/media.html" >
    <i class="fas fa-folder-open fa-fw fa-lg"></i> Media</a></li>
  <li><a href="/category/misc.html" >
    <i class="fas fa-folder-open fa-fw fa-lg"></i> misc</a></li>
  <li><a href="/category/phone.html" >
    <i class="fas fa-folder-open fa-fw fa-lg"></i> Phone</a></li>
  <li><a href="/category/privacy.html" >
    <i class="fas fa-folder-open fa-fw fa-lg"></i> Privacy</a></li>
  <li><a href="/category/security.html" >
    <i class="fas fa-folder-open fa-fw fa-lg"></i> Security</a></li>
  <li><a href="/category/software.html" >
    <i class="fas fa-folder-open fa-fw fa-lg"></i> Software</a></li>
</ul>
</div>

</div> <!-- /row -->

  <h4><i class="fas fa-link fa-fw fa-lg"></i> Links</h4>
  <ul class="list-unstyled category-links">
    <li><a href="https://getpelican.com/" >
      <i class="fas fa-external-link-square-alt fa-fw fa-lg"></i> Pelican</a></li>
    <li><a href="https://www.python.org/" >
      <i class="fas fa-external-link-square-alt fa-fw fa-lg"></i> Python.org</a></li>
    <li><a href="https://palletsprojects.com/p/jinja/" >
      <i class="fas fa-external-link-square-alt fa-fw fa-lg"></i> Jinja2</a></li>
  </ul>
<h4><i class="fas fa-tags fa-fw fa-lg"></i> Tags</h4>
<p class="tag-cloud">
      <a href="/tag/linux.html">
        <i class="fas fa-tag fa-fw fa-lg"></i>linux
      </a>
      <a href="/tag/os.html">
        <i class="fas fa-tag fa-fw fa-lg"></i>os
      </a>
      <a href="/tag/fedora.html">
        <i class="fas fa-tag fa-fw fa-lg"></i>fedora
      </a>
      <a href="/tag/networking.html">
        <i class="fas fa-tag fa-fw fa-lg"></i>networking
      </a>
      <a href="/tag/wireguard.html">
        <i class="fas fa-tag fa-fw fa-lg"></i>wireguard
      </a>
      <a href="/tag/privacy.html">
        <i class="fas fa-tag fa-fw fa-lg"></i>privacy
      </a>
      <a href="/tag/media.html">
        <i class="fas fa-tag fa-fw fa-lg"></i>media
      </a>
      <a href="/tag/hardware.html">
        <i class="fas fa-tag fa-fw fa-lg"></i>hardware
      </a>
      <a href="/tag/software.html">
        <i class="fas fa-tag fa-fw fa-lg"></i>software
      </a>
      <a href="/tag/smarthome.html">
        <i class="fas fa-tag fa-fw fa-lg"></i>smarthome
      </a>
      <a href="/tag/sustainability.html">
        <i class="fas fa-tag fa-fw fa-lg"></i>sustainability
      </a>
      <a href="/tag/mqtt.html">
        <i class="fas fa-tag fa-fw fa-lg"></i>mqtt
      </a>
      <a href="/tag/photography.html">
        <i class="fas fa-tag fa-fw fa-lg"></i>photography
      </a>
      <a href="/tag/security.html">
        <i class="fas fa-tag fa-fw fa-lg"></i>security
      </a>
      <a href="/tag/mobile.html">
        <i class="fas fa-tag fa-fw fa-lg"></i>mobile
      </a>
      <a href="/tag/quicktip.html">
        <i class="fas fa-tag fa-fw fa-lg"></i>quicktip
      </a>
      <a href="/tag/sun.html">
        <i class="fas fa-tag fa-fw fa-lg"></i>sun
      </a>
      <a href="/tag/arduino.html">
        <i class="fas fa-tag fa-fw fa-lg"></i>arduino
      </a>
      <a href="/tag/web.html">
        <i class="fas fa-tag fa-fw fa-lg"></i>web
      </a>
      <a href="/tag/bookmarks.html">
        <i class="fas fa-tag fa-fw fa-lg"></i>bookmarks
      </a>
      <a href="/tag/documentation.html">
        <i class="fas fa-tag fa-fw fa-lg"></i>documentation
      </a>
      <a href="/tag/blog.html">
        <i class="fas fa-tag fa-fw fa-lg"></i>blog
      </a>
      <a href="/tag/housekeeping.html">
        <i class="fas fa-tag fa-fw fa-lg"></i>housekeeping
      </a>
      <a href="/tag/markdown.html">
        <i class="fas fa-tag fa-fw fa-lg"></i>markdown
      </a>
      <a href="/tag/cloud.html">
        <i class="fas fa-tag fa-fw fa-lg"></i>cloud
      </a>
      <a href="/tag/spyware.html">
        <i class="fas fa-tag fa-fw fa-lg"></i>spyware
      </a>
      <a href="/tag/phone.html">
        <i class="fas fa-tag fa-fw fa-lg"></i>phone
      </a>
      <a href="/tag/applications.html">
        <i class="fas fa-tag fa-fw fa-lg"></i>applications
      </a>
      <a href="/tag/chat.html">
        <i class="fas fa-tag fa-fw fa-lg"></i>chat
      </a>
      <a href="/tag/mastodon.html">
        <i class="fas fa-tag fa-fw fa-lg"></i>mastodon
      </a>
      <a href="/tag/gui.html">
        <i class="fas fa-tag fa-fw fa-lg"></i>gui
      </a>
      <a href="/tag/application.html">
        <i class="fas fa-tag fa-fw fa-lg"></i>application
      </a>
</p>

<hr />

        </div><!--/sidebar -->
      </div><!--/row-->
    </div><!--/.container /#main-container -->

    <footer id="site-footer">
 
      <address id="site-colophon">
        <p class="text-center text-muted">
        Site built using <a href="http://getpelican.com/" target="_blank">Pelican</a>
        &nbsp;&bull;&nbsp; Theme based on
        <a href="http://www.voidynullness.net/page/voidy-bootstrap-pelican-theme/"
           target="_blank">VoidyBootstrap</a> by 
        <a href="http://voidynullness.net"
           target="_blank">RKI</a>  
        </p>
      </address><!-- /colophon  -->
    </footer>


    <!-- javascript -->
   
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js" integrity="sha384-nvAa0+6Qg9clwYCGGPpDQLVpLNn0fRaROjHqs13t4Ggj3Ez50XnGQqc/r8MhnRDZ" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
            integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
            crossorigin="anonymous"></script>


  </body>
</html>