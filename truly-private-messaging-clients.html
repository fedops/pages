<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta name="description" content="" />
    <meta name="author" content="fedops" />
    <meta name="generator" content="Pelican (VoidyBootstrap theme)" />

    <title>Truly Private Messaging Clients - fedops blog</title>

   
        <link rel="stylesheet"
              href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
              integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u"
              crossorigin="anonymous" />

      <link rel="stylesheet"
            href="https://use.fontawesome.com/releases/v5.0.13/css/all.css"
            integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp"
            crossorigin="anonymous"
      />




      <link rel="stylesheet" href="/theme/css/pygment.css" />
      <link rel="stylesheet" href="/theme/css/voidybootstrap.css" />

    <link rel="shortcut icon" href="/favicon.ico" />
  </head>

  <body>
   
    <nav class="navbar navbar-default">
      <div class="container">
	   <div class="navbar-header">
		<button type="button" class="navbar-toggle" 
				data-toggle="collapse" data-target="#main-navbar-collapse">
		  <span class="sr-only">Toggle navigation</span>
		  <span class="icon-bar"></span>
		  <span class="icon-bar"></span>
		  <span class="icon-bar"></span>
		</button>
		<a class="navbar-brand" href="/" rel="home">
          <i class="fas fa-home fa-fw fa-lg"> </i> </a>
       </div>

      <div class="collapse navbar-collapse" id="main-navbar-collapse">
        <ul class="nav navbar-nav">
              <li>
                <a href="/pages/about-this-blog.html">About this Blog</a>
              </li>
            <li class="divider"></li>
            <li class="">
              <a href="/archives.html">Archives</a>
            </li>
          <li class="divider"></li>
        </ul> <!-- /nav -->
      </div> <!-- /navbar-collapse -->
	  </div> <!-- /container -->
    </nav> <!-- /navbar -->

	<div class="jumbotron" id="overview">
	  <div class="container">
		<h1><a href="/">fedops blog</a></h1>
		<p class="lead">Privacy in Computing</p>
	  </div>
	</div>

    <div class="container" id="main-container">
      <div class="row">
        <div class="col-md-9" id="content">
<article itemscope="itemscope" itemtype="http://schema.org/BlogPosting">
  <header class="article-header">
<abbr class="article-header-date">
  Tue 13 April 2021
</abbr> <h1>
  <a href="/truly-private-messaging-clients.html" rel="bookmark"
     title="Permalink to Truly Private Messaging Clients">
    Truly Private Messaging Clients
  </a>
</h1><div class="article-header-info">
  <p>
      Posted by <a href="/author/fedops.html">fedops</a>
    in 
    <a href="/category/privacy.html">
      Privacy</a>
    &nbsp;&nbsp;
  </p>
</div> <!-- /.article-header-info -->  </header>
  <div class="content-body" itemprop="text articleBody">
	<p><em>This post was updated with feedback</em></p>
<h1 id="chat-or-messaging">Chat or Messaging?</h1>
<p>First of all it is useful to make a distinction between messaging and chat
clients. Those two use cases get thrown into the same bag a lot, but the
requirements are actually quite different.</p>
<h2 id="messaging">Messaging</h2>
<p>Messaging clients are intended to provide a direct communications link between
two people, sometimes with an option of expanding this to a (small) group of
people. In both cases the members know each other on a first hand basis.
Chances are they are in each others' address books or contact lists. They do
not seek to meet new people. They merely want to establish a channel between
previously-known people.</p>
<p>What counts is positive identification of who is on the other end, with a
notification if what happens is not what's expected. The contents of the
communications are private and must remain so, also into the future. The privacy
requirement also extends to incidental metadata, such as message transaction
logs and relationship graphs.</p>
<p>Extending the communications beyond mere texting and media attachments, a
requirement is often raised to also support voice and video calling.</p>
<p>Messaging can be server-based or direct peer-to-peer, or a mixture of both.</p>
<h2 id="chatting">Chatting</h2>
<p>Chat clients, on the other hand, are tools used by people who would like to
participate in losely-knit groups of people discussing topics of mutual
interest.</p>
<p>Predominantly the participants do not know each other face to face, and
membership of the groups will fluctuate over time as people lose interest and
new members join. Some of these groups reach hundreds, even thousands of
members.</p>
<p>Usually chat groups will be hosted on servers, either standalone or federated.
Due to this it is almost impossible to control the spread of metadata as every
server must out of necessity be trusted to properly care for its privacy.
Message contents can be encrypted, but that will usually be client-to-server,
not end-to-end.</p>
<h1 id="whats-a-truly-private-messenger">What's a "truly private messenger"?</h1>
<p>Now with those definitions out of the way, here are my thoughts on messaging
clients.</p>
<h2 id="encryption">Encryption</h2>
<p>This is the most obvious requirement: encrypt the payload end-to-end so that
only the sender and the intended recipient(s) have access. Besides the actual
algorithm used, factors such as plausible deniability and forward secrecy have
to be taken into account.</p>
<p>Agreement on encryption keys can be a massive showstopper for widespread
acceptance, often dominated by the perceived ease of use. Trust-on-first-use
tends to be the "easy button" though it comes with serious security drawbacks.</p>
<p>It's nice if TOFU as well as manual keying is supported.</p>
<h2 id="server-infrastructure">Server Infrastructure</h2>
<p>Modern messaging protocols do not need central server infrastructure. These are
rightfully labeled peer to peer messengers, and are vastly superior to anything
that relies on infrastructure, especially when that is centralized and run by a
single entity.</p>
<p>This infrastructure is liable to government intervention such as wiretapping and
subpoenas. It can be taken offline or otherwise fail at any point. And it is
hard to control metadata when all messages travel through a single place on the
net.</p>
<p>Steer well clear of these systems.</p>
<h2 id="user-identification">User Identification</h2>
<p>As opposed to chat systems, which rely on users being able to find other users,
messaging clients don't have to rely on such functionality since participants
tend to already know each other. It can be a bonus addon, which makes it easier
to add peers to the personal address book, but as messengers such as Session
demonstrate it is perfectly possible to add other people simply by copying and
pasting long and obscure hashes or scan QR codes.</p>
<p>Prefer messengers that make user identification optional, and be very wary of
ones that require central registration.</p>
<h2 id="metadata">Metadata</h2>
<p>It is surprising how much information can be gleaned merely from the metadata
surrounding communication events without the actual message contents actually
being captured. </p>
<p>Information such as a graph of personal relationships; frequency and timing of
messages between individuals indicating their relationship status; message size
indicating whether media is exchanged; etc. all might give away evidence that in
some cases makes the actual content of the messages irrelevant.</p>
<p>Peer to peer systems can be very hard to capture such metadata of, especially if
they use some form of obfuscated transport (such as onion routing) to deliver
messages.</p>
<h1 id="recommendations">Recommendations</h1>
<p>Keeping the above in mind, my own personal recommendations for messengers as of
right now are the following, in no particular order of preference.</p>
<h2 id="session">Session</h2>
<p>A virtual lookalike to Signal, but serverless and with a form of onion
routing. Clients are very "mainstream-y" (which is good), but have some odd
niggles which may hamper adoption by non-techies.</p>
<p>For example you can only share addresses with others by using the
system-generated hashes as there is no discovery function for peers.  The
interface is very sparse, and some of the options are rather hidden; such as
renaming a contact by having to long-press on the avatar, then entering details,
and finally clicking the edit pencil.</p>
<p>Detracting from the otherwise positive impressions are a business model for the
swarm operators that is based on cryptocurrency exchange, and the possibility of
a "Pro" version that is somehow offering services above the "Free" tier. As of
early 2022 it is unclear what exactly this will mean.</p>
<p><a href="https://getsession.org/">Session</a></p>
<h2 id="tox">Tox</h2>
<p>Tox is a very interesting protocol, which is published as a set of libraries
with no central institution operating any kind of service around it. A number of
good client implementations exist. For Android specifically <a href="https://github.com/zoff99/ToxAndroidRefImpl">Trifa is very
nice</a>.</p>
<p>A feature of the completely serverless implementation is that both endpoints to
a conversation have to be simultaneously online. This harks back to the IRC of
old, and indeed the solution/workaround is similar: use a proxy. As explained on
the <a href="https://wiki.tox.chat/users/offline_messaging">Offline Messaging Wiki page</a>
ToxProxy can be used -- but only with Trifa at the moment. All other clients
implement a pseudo-offline function in that they buffer messages to be sent
transparently, but still sender and receiver need to have overlapping online
time for the messages to actually be sent.</p>
<p>Lack of central marketing is another limiting factor for Tox, leading to very poor
public awareness of the protocol outside of the nerdspace.</p>
<p><a href="https://tox.chat/">Tox</a></p>
<h2 id="jami">Jami</h2>
<p>With its official Gnu project status and an architecture that encourages plugin
development, Jami is a very streamlined and "normie-friendly" approach which is
also rather feature rich - almost the antithesis of Session which is reduced to
the max. Unfortunately especially the mobile clients have a reputation for rough
edges and somewhat unreliable operation. Quite a bit of finishing work remains
to be done.</p>
<p>Creation of a user id on directory servers is optional, though might be useful
to boost acceptance. All three server types can be run self-hosted if so
desired, and in any event only the Turn server is required if NAT is in use
(which these days it almost invariably is).</p>
<p><a href="https://jami.net/">Jami</a></p>
<h2 id="others">Others</h2>
<p>A number of other systems exist, some of them quite technically interesting such
as <a href="https://en.wikipedia.org/wiki/Secure_Scuttlebutt">Secure Scuttlebutt (SSB)</a>.</p>
<p>I will be following up with a new edition of this post as new material becomes
available.</p>
<h1 id="non-starters">Non-Starters</h1>
<p>Just very briefly:</p>
<p><strong>Briar</strong>: no iOS client means acceptance will be very limited. Does not provide
a peer discovery function except when physically present in the same Wifi network
or within bluetooth range. With iOS support this would be a very strong
contender, or put another way: if you don't need to support Apple users this
might be the platform for you.</p>
<p><strong>Wire/Threema</strong>: central closed-source server infrastructure.</p>
<p><strong>Signal</strong>: central server infrastructure, which is "somewhat" open source (search
for the flame wars having erupted on this subject in early 2021).</p>
<p><strong>Telegram</strong>: central closed-source server infrastructure, proprietary encryption
protocol, unclear business model, lots of other problems. Features prominently on
the radar of law enforcement agencies due to lots of criminal activity in
various "dark" Telegram groups.</p>
<h1 id="summary">Summary</h1>
<p>All three of the recommendations, plus Briar, operate without complete reliance on a central
infrastructure (Jami's Turn servers excluded) and leave little to no metadata
traces. You won't go wrong with either.</p>
<p>Other than Session, both Tox as well as Jami support additional features beyond
messaging; namely, voice and video calls. As of 2022, Session is strictly for
person to person or group chat text messaging - including file attachments -
only.</p>
<p>My personal recommendation is: Tox, if you can get your circle of communication
partners to move to it. </p>
<p>If not, Jami or Session both of which might be an easier sell. Jami has more
bells and whistles but is still somewhat unreliable, Session looks almost like
Signal but with a reduced feature set. If you don't know anyone with an Apple
device, Briar is worth a look too.</p>
<p>The most <em>practical</em> option has been for years and remains so, Signal. But it
does come with the limitation of the centralized infrastructure, albeit the
least privacy-intrusive of all of them.</p>
  </div>
  
<div class="article-tag-list">
<span class="label label-default">Tags</span>
	<a href="/tag/chat.html"><i class="fas fa-tag"></i>chat</a>&nbsp;
	<a href="/tag/software.html"><i class="fas fa-tag"></i>software</a>&nbsp;
	<a href="/tag/privacy.html"><i class="fas fa-tag"></i>privacy</a>&nbsp;
</div><!-- via neighbor plugin, see: https://github.com/pelican-plugins/neighbors -->
    <hr />
    <p class="content-emphasis">
	<table width="100%">
	    <tr>
			<td align="left">
	        </td>
	        <td align="right">
	        </td>
		</tr>
	</table>
	</p>
</article>
        </div><!-- /content -->

        <div class="col-md-3 sidebar-nav" id="sidebar">

<div class="row">

<div class="col-xs-6 col-md-12">
<h4><i class="fas fa-comment fa-fw fa-lg"></i> Social</h4>
<ul class="list-unstyled social-links">
    <li><a href="https://fosstodon.org/@fedops" target="_blank">
	  <i class="fas fa-comments fa-fw fa-lg" title="Fosstodon"></i>
		Fosstodon
	</a></li>
    <li><a href="https://codeberg.org/fedops" target="_blank">
	  <i class="fas fa-comments fa-fw fa-lg" title="Codeberg"></i>
		Codeberg
	</a></li>
</ul>
</div>

<div class="col-xs-6 col-md-12">
<h4><i class="fas fa-folder fa-fw fa-lg"></i> Categories</h4>
<ul class="list-unstyled category-links">
  <li><a href="/category/cloud.html" >
    <i class="fas fa-folder-open fa-fw fa-lg"></i> Cloud</a></li>
  <li><a href="/category/hardware.html" >
    <i class="fas fa-folder-open fa-fw fa-lg"></i> Hardware</a></li>
  <li><a href="/category/howto.html" >
    <i class="fas fa-folder-open fa-fw fa-lg"></i> Howto</a></li>
  <li><a href="/category/infomanagement.html" >
    <i class="fas fa-folder-open fa-fw fa-lg"></i> InfoManagement</a></li>
  <li><a href="/category/media.html" >
    <i class="fas fa-folder-open fa-fw fa-lg"></i> Media</a></li>
  <li><a href="/category/misc.html" >
    <i class="fas fa-folder-open fa-fw fa-lg"></i> misc</a></li>
  <li><a href="/category/phone.html" >
    <i class="fas fa-folder-open fa-fw fa-lg"></i> Phone</a></li>
  <li><a href="/category/privacy.html" >
    <i class="fas fa-folder-open fa-fw fa-lg"></i> Privacy</a></li>
  <li><a href="/category/security.html" >
    <i class="fas fa-folder-open fa-fw fa-lg"></i> Security</a></li>
  <li><a href="/category/software.html" >
    <i class="fas fa-folder-open fa-fw fa-lg"></i> Software</a></li>
</ul>
</div>

</div> <!-- /row -->

  <h4><i class="fas fa-link fa-fw fa-lg"></i> Links</h4>
  <ul class="list-unstyled category-links">
    <li><a href="https://getpelican.com/" >
      <i class="fas fa-external-link-square-alt fa-fw fa-lg"></i> Pelican</a></li>
    <li><a href="https://www.python.org/" >
      <i class="fas fa-external-link-square-alt fa-fw fa-lg"></i> Python.org</a></li>
    <li><a href="https://palletsprojects.com/p/jinja/" >
      <i class="fas fa-external-link-square-alt fa-fw fa-lg"></i> Jinja2</a></li>
  </ul>
<h4><i class="fas fa-tags fa-fw fa-lg"></i> Tags</h4>
<p class="tag-cloud">
      <a href="/tag/linux.html">
        <i class="fas fa-tag fa-fw fa-lg"></i>linux
      </a>
      <a href="/tag/os.html">
        <i class="fas fa-tag fa-fw fa-lg"></i>os
      </a>
      <a href="/tag/fedora.html">
        <i class="fas fa-tag fa-fw fa-lg"></i>fedora
      </a>
      <a href="/tag/networking.html">
        <i class="fas fa-tag fa-fw fa-lg"></i>networking
      </a>
      <a href="/tag/wireguard.html">
        <i class="fas fa-tag fa-fw fa-lg"></i>wireguard
      </a>
      <a href="/tag/privacy.html">
        <i class="fas fa-tag fa-fw fa-lg"></i>privacy
      </a>
      <a href="/tag/media.html">
        <i class="fas fa-tag fa-fw fa-lg"></i>media
      </a>
      <a href="/tag/hardware.html">
        <i class="fas fa-tag fa-fw fa-lg"></i>hardware
      </a>
      <a href="/tag/software.html">
        <i class="fas fa-tag fa-fw fa-lg"></i>software
      </a>
      <a href="/tag/smarthome.html">
        <i class="fas fa-tag fa-fw fa-lg"></i>smarthome
      </a>
      <a href="/tag/sustainability.html">
        <i class="fas fa-tag fa-fw fa-lg"></i>sustainability
      </a>
      <a href="/tag/mqtt.html">
        <i class="fas fa-tag fa-fw fa-lg"></i>mqtt
      </a>
      <a href="/tag/photography.html">
        <i class="fas fa-tag fa-fw fa-lg"></i>photography
      </a>
      <a href="/tag/security.html">
        <i class="fas fa-tag fa-fw fa-lg"></i>security
      </a>
      <a href="/tag/mobile.html">
        <i class="fas fa-tag fa-fw fa-lg"></i>mobile
      </a>
      <a href="/tag/quicktip.html">
        <i class="fas fa-tag fa-fw fa-lg"></i>quicktip
      </a>
      <a href="/tag/sun.html">
        <i class="fas fa-tag fa-fw fa-lg"></i>sun
      </a>
      <a href="/tag/arduino.html">
        <i class="fas fa-tag fa-fw fa-lg"></i>arduino
      </a>
      <a href="/tag/web.html">
        <i class="fas fa-tag fa-fw fa-lg"></i>web
      </a>
      <a href="/tag/bookmarks.html">
        <i class="fas fa-tag fa-fw fa-lg"></i>bookmarks
      </a>
      <a href="/tag/documentation.html">
        <i class="fas fa-tag fa-fw fa-lg"></i>documentation
      </a>
      <a href="/tag/blog.html">
        <i class="fas fa-tag fa-fw fa-lg"></i>blog
      </a>
      <a href="/tag/housekeeping.html">
        <i class="fas fa-tag fa-fw fa-lg"></i>housekeeping
      </a>
      <a href="/tag/markdown.html">
        <i class="fas fa-tag fa-fw fa-lg"></i>markdown
      </a>
      <a href="/tag/cloud.html">
        <i class="fas fa-tag fa-fw fa-lg"></i>cloud
      </a>
      <a href="/tag/spyware.html">
        <i class="fas fa-tag fa-fw fa-lg"></i>spyware
      </a>
      <a href="/tag/phone.html">
        <i class="fas fa-tag fa-fw fa-lg"></i>phone
      </a>
      <a href="/tag/applications.html">
        <i class="fas fa-tag fa-fw fa-lg"></i>applications
      </a>
      <a href="/tag/chat.html">
        <i class="fas fa-tag fa-fw fa-lg"></i>chat
      </a>
      <a href="/tag/mastodon.html">
        <i class="fas fa-tag fa-fw fa-lg"></i>mastodon
      </a>
      <a href="/tag/gui.html">
        <i class="fas fa-tag fa-fw fa-lg"></i>gui
      </a>
      <a href="/tag/application.html">
        <i class="fas fa-tag fa-fw fa-lg"></i>application
      </a>
</p>

<hr />

        </div><!--/sidebar -->
      </div><!--/row-->
    </div><!--/.container /#main-container -->

    <footer id="site-footer">
 
      <address id="site-colophon">
        <p class="text-center text-muted">
        Site built using <a href="http://getpelican.com/" target="_blank">Pelican</a>
        &nbsp;&bull;&nbsp; Theme based on
        <a href="http://www.voidynullness.net/page/voidy-bootstrap-pelican-theme/"
           target="_blank">VoidyBootstrap</a> by 
        <a href="http://voidynullness.net"
           target="_blank">RKI</a>  
        </p>
      </address><!-- /colophon  -->
    </footer>


    <!-- javascript -->
   
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js" integrity="sha384-nvAa0+6Qg9clwYCGGPpDQLVpLNn0fRaROjHqs13t4Ggj3Ez50XnGQqc/r8MhnRDZ" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
            integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
            crossorigin="anonymous"></script>


  </body>
</html>