<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta name="description" content="" />
    <meta name="author" content="fedops" />
    <meta name="generator" content="Pelican (VoidyBootstrap theme)" />

    <title>Gnome Tracker Issues - fedops blog</title>

   
        <link rel="stylesheet"
              href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
              integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u"
              crossorigin="anonymous" />

      <link rel="stylesheet"
            href="https://use.fontawesome.com/releases/v5.0.13/css/all.css"
            integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp"
            crossorigin="anonymous"
      />




      <link rel="stylesheet" href="/theme/css/pygment.css" />
      <link rel="stylesheet" href="/theme/css/voidybootstrap.css" />

    <link rel="shortcut icon" href="/favicon.ico" />
  </head>

  <body>
   
    <nav class="navbar navbar-default">
      <div class="container">
	   <div class="navbar-header">
		<button type="button" class="navbar-toggle" 
				data-toggle="collapse" data-target="#main-navbar-collapse">
		  <span class="sr-only">Toggle navigation</span>
		  <span class="icon-bar"></span>
		  <span class="icon-bar"></span>
		  <span class="icon-bar"></span>
		</button>
		<a class="navbar-brand" href="/" rel="home">
          <i class="fas fa-home fa-fw fa-lg"> </i> </a>
       </div>

      <div class="collapse navbar-collapse" id="main-navbar-collapse">
        <ul class="nav navbar-nav">
              <li>
                <a href="/pages/about-this-blog.html">About this Blog</a>
              </li>
            <li class="divider"></li>
            <li class="">
              <a href="/archives.html">Archives</a>
            </li>
          <li class="divider"></li>
        </ul> <!-- /nav -->
      </div> <!-- /navbar-collapse -->
	  </div> <!-- /container -->
    </nav> <!-- /navbar -->

	<div class="jumbotron" id="overview">
	  <div class="container">
		<h1><a href="/">fedops blog</a></h1>
		<p class="lead">Privacy in Computing</p>
	  </div>
	</div>

    <div class="container" id="main-container">
      <div class="row">
        <div class="col-md-9" id="content">
<article itemscope="itemscope" itemtype="http://schema.org/BlogPosting">
  <header class="article-header">
<abbr class="article-header-date">
  Sun 29 July 2018
</abbr> <h1>
  <a href="/gnome-tracker-issues.html" rel="bookmark"
     title="Permalink to Gnome Tracker Issues">
    Gnome Tracker Issues
  </a>
</h1><div class="article-header-info">
  <p>
      Posted by <a href="/author/fedops.html">fedops</a>
    in 
    <a href="/category/software.html">
      Software</a>
    &nbsp;&nbsp;
  </p>
</div> <!-- /.article-header-info -->  </header>
  <div class="content-body" itemprop="text articleBody">
	<p>Modern desktop environments feature functionality that enables users to easily
locate files based on metadata as well as contents. Arguably the first
successful implementation of a data scraper and integration into the desktop was
Apple's Spotlight. Several of the Linux desktop environments feature similar
software, among them <a href="https://wiki.gnome.org/Projects/Tracker">Gnome's Tracker</a>.</p>
<h3 id="privacy-implications">Privacy Implications</h3>
<p>By design, Tracker (and pretty much all other indexers) collect information
about your files in their own database for later quick reference when you
execute searches. By default this information is stored in
<code>$HOME/.cache/tracker</code>. From a privacy point of view there are a number of
takeaways:</p>
<ul>
<li>potentially sensitive data is stored in a location separate from the actual
  files. Deleting one doesn't necessarily delete the other.</li>
<li>there is a time lag between changing file contents and the Tracker miner
  coming along to also update the cache.</li>
<li>as a combination of the above two, understand the fact that creating a file on
  disk, then encrypting it and deleting the unencrypted original, can leave
  behind traces of the unencrypted cleartext for a significant amount of time.
  In fact, it is up to the actual implementation of the database used for
  caching whether this information will ever be deleted.</li>
</ul>
<p>As always, user friendliness comes with tradeoffs.</p>
<p>If you are concerned about this, consider excluding certain areas of your file
systems from the indexing process, or disabling (and potentially deinstalling)
the entire mechanism.</p>
<p><em>Note that temporary files on disk pose a risk even without indexing like this.
Creating a file and then simply deleting it will leave behind the information
contained within the file. Deleting a file is actually just an unlink() call to
the operating system, removing the file's entry in the directory structure and
marking its blocks on disk as free. Until these blocks are actually overwritten
with new files, their information remains recoverable. Therefore, a secure erase
function such as shred(1) needs to be employed to actually get rid of the
contents.</em></p>
<h3 id="operational-notes">Operational Notes</h3>
<p>From a practical implementation point of view, Tracker usually runs in a very
unobtrusive way. It comes with multiple processes which can be viewed using:</p>
<pre><code>$ tracker daemon status
Store:
29 Jul 2018, 16:20:56:    0%  Store                   - Idle 

Miners:
29 Jul 2018, 16:20:56:    1%  File System             - Crawling recursively directory 'file:///home/bla/Pictures' 
29 Jul 2018, 16:20:56:  ✗     Extractor               - Not running or is a disabled plugin
29 Jul 2018, 16:20:56:  ✓     Applications            - Idle 
29 Jul 2018, 16:20:56:  ✓     RSS/ATOM Feeds          - Idle 
</code></pre>
<p>The data miner for the file system, called <code>tracker-miner-fs</code>, crawls through
the available mounted file system(s) looking for new and changed files, and
hands them over to <code>tracker-extract</code> which digs through them for indexable
information. These processes are niced to -19, which means they will utilize
whatever CPU they might require but will never compete with anything else. This
is all fine and dandy and usually works well.</p>
<p>Apart from the desktop integration, users are given a number of commandline
functions to query and manipulate data. <code>tracker info &lt;file&gt;</code> displays all info
Tracker has amassed about a file. <code>tracker extract &lt;file&gt;</code> causes Tracker to
extract whatever it can find in a file.</p>
<p>Apart from the factual file data and metadata, such as for example GPS
coordinates and resolution in a photo, arbitrary keywords called tags can be
created and assigned to files. For example:</p>
<pre><code># add a tage to a file, creating the tag if necessary
$ tracker tag -a screenshot Pictures/tracker.png 
Tag was added successfully
  Tagged: file:///home/bla/Pictures/tracker.png
# show all tags on a given file
$ tracker tag Pictures/tracker.png 
file:///home/bla/Pictures/tracker.png
  screenshot
# show all files with a given tag
$ tracker tag -s -t screenshot
Tags (shown by name):
  screenshot 
    file:///home/bla/Pictures/tracker.png
</code></pre>
<p>At the time of this post, there doesn't seem to be a mechanism to correlate e.g.
darktable tags written to XMP sidecar files with Tracker tags. In fact, Tracker
has no predefined way to parse these specific XML files, though given the open
nature of the system one could certainly be written.</p>
<h3 id="problems">Problems</h3>
<p>Sometimes, <code>tracker-extract</code> will choke on certain files and crash. The whole
process will then enter an endless loop trying to re-index the file(s), crash
again, and so on. The usual way to identify this is the fact that the extractor
periodically climbs to the top of the CPU usage statistics:</p>
<pre><code>$ top

top - 16:23:02 up  2:46,  3 users,  load average: 1.03, 1.03, 0.88
Tasks: 334 total,   4 running, 250 sleeping,   0 stopped,   0 zombie
%Cpu(s):  4.1 us,  0.9 sy, 11.3 ni, 83.5 id,  0.0 wa,  0.1 hi,  0.1 si,  0.0 st
KiB Mem : 16300324 total,   157680 free,  2595000 used, 13547644 buff/cache
KiB Swap:  8220668 total,  8141308 free,    79360 used. 12959260 avail Mem 

  PID USER      PR  NI    VIRT    RES    SHR S  %CPU %MEM     TIME+ COMMAND 
10720 bla       39  19 1968536  49412  28052 R  92.4  0.3   0:02.78 tracker-extract
</code></pre>
<p>There will also be telltale messages in the system log:</p>
<pre><code>$ journalctl -f
-- Logs begin at Wed 2018-01-10 12:30:34 CET. --
Jul 29 16:24:32 airtuxi.mtnsub.org tracker-extract[10876]: Could not insert metadata for item &quot;file:///home/bla/Documents/something.jpg&quot;: 89.19: invalid UTF-8 character
Jul 29 16:24:32 airtuxi.mtnsub.org tracker-extract[10876]: If the error above is recurrent for the same item/ID, consider running &quot;tracker-extract&quot; in the terminal with the TRACKER_VERBOSITY=3 environment variable, and filing a bug with the additional information
Jul 29 16:24:32 airtuxi.mtnsub.org tracker-extract[10876]: Could not insert metadata for item &quot;file:///home/bla/Documents/else.pdf&quot;: 24.32: invalid UTF-8 character
Jul 29 16:24:32 airtuxi.mtnsub.org tracker-extract[10876]: If the error above is recurrent for the same item/ID, consider running &quot;tracker-extract&quot; in the terminal with the TRACKER_VERBOSITY=3 environment variable, and filing a bug with the additional information
Jul 29 16:24:32 airtuxi.mtnsub.org tracker-extract[10876]: Could not insert metadata for item &quot;file:///home/bla/Documents/foobar.pdf&quot;: 24.32: invalid UTF-8 character
Jul 29 16:24:32 airtuxi.mtnsub.org tracker-extract[10876]: If the error above is recurrent for the same item/ID, consider running &quot;tracker-extract&quot; in the terminal with the TRACKER_VERBOSITY=3 environment variable, and filing a bug with the additional information
</code></pre>
<p>This will basically loop indefinitely until tracker execution is stopped via
<code>tracker daemon -t</code>. So far these issues seem to be caused by (and limited to)
files with embedded oddball UTF-8 characters. Either way it would be useful to
add a flag that tells Tracker about an incompatible file and causes it to mark
this file as excluded after a number of failed attempts to index it. Cue
<a href="https://en.wikipedia.org/wiki/Graceful_degradation">Graceful Degradation</a>.</p>
<p>There <a href="https://bugzilla.redhat.com/show_bug.cgi?id=1602068">is a bug filed for
this</a> and hopefully a fix
will be available soon. Meanwhile - if there are only a few files causing
problems - it may help to exclude directories with these files from the
harvesting operations:</p>
<pre><code># list all Tracker settings
$ gsettings list-recursively | grep Tracker
[...]
org.freedesktop.Tracker.Miner.Files index-on-battery true
org.freedesktop.Tracker.Miner.Files sched-idle 'first-index'
org.freedesktop.Tracker.Miner.Files ignored-directories ['po', 'CVS', 'core-dumps', 'lost+found']
org.freedesktop.Tracker.Miner.Files crawling-interval -1
[...]
# add &quot;Documents&quot; to exclusions
$ gsettings set org.freedesktop.Tracker.Miner.Files ignored-directories &quot;['some/directory', 'po', 'CVS', 'core-dumps', 'lost+found']&quot;
</code></pre>
  </div>
  
<div class="article-tag-list">
<span class="label label-default">Tags</span>
	<a href="/tag/os.html"><i class="fas fa-tag"></i>os</a>&nbsp;
</div><!-- via neighbor plugin, see: https://github.com/pelican-plugins/neighbors -->
    <hr />
    <p class="content-emphasis">
	<table width="100%">
	    <tr>
			<td align="left">
	        </td>
	        <td align="right">
	        </td>
		</tr>
	</table>
	</p>
</article>
        </div><!-- /content -->

        <div class="col-md-3 sidebar-nav" id="sidebar">

<div class="row">

<div class="col-xs-6 col-md-12">
<h4><i class="fas fa-comment fa-fw fa-lg"></i> Social</h4>
<ul class="list-unstyled social-links">
    <li><a href="https://fosstodon.org/@fedops" target="_blank">
	  <i class="fas fa-comments fa-fw fa-lg" title="Fosstodon"></i>
		Fosstodon
	</a></li>
    <li><a href="https://codeberg.org/fedops" target="_blank">
	  <i class="fas fa-comments fa-fw fa-lg" title="Codeberg"></i>
		Codeberg
	</a></li>
</ul>
</div>

<div class="col-xs-6 col-md-12">
<h4><i class="fas fa-folder fa-fw fa-lg"></i> Categories</h4>
<ul class="list-unstyled category-links">
  <li><a href="/category/cloud.html" >
    <i class="fas fa-folder-open fa-fw fa-lg"></i> Cloud</a></li>
  <li><a href="/category/hardware.html" >
    <i class="fas fa-folder-open fa-fw fa-lg"></i> Hardware</a></li>
  <li><a href="/category/howto.html" >
    <i class="fas fa-folder-open fa-fw fa-lg"></i> Howto</a></li>
  <li><a href="/category/infomanagement.html" >
    <i class="fas fa-folder-open fa-fw fa-lg"></i> InfoManagement</a></li>
  <li><a href="/category/media.html" >
    <i class="fas fa-folder-open fa-fw fa-lg"></i> Media</a></li>
  <li><a href="/category/misc.html" >
    <i class="fas fa-folder-open fa-fw fa-lg"></i> misc</a></li>
  <li><a href="/category/phone.html" >
    <i class="fas fa-folder-open fa-fw fa-lg"></i> Phone</a></li>
  <li><a href="/category/privacy.html" >
    <i class="fas fa-folder-open fa-fw fa-lg"></i> Privacy</a></li>
  <li><a href="/category/security.html" >
    <i class="fas fa-folder-open fa-fw fa-lg"></i> Security</a></li>
  <li><a href="/category/software.html" >
    <i class="fas fa-folder-open fa-fw fa-lg"></i> Software</a></li>
</ul>
</div>

</div> <!-- /row -->

  <h4><i class="fas fa-link fa-fw fa-lg"></i> Links</h4>
  <ul class="list-unstyled category-links">
    <li><a href="https://getpelican.com/" >
      <i class="fas fa-external-link-square-alt fa-fw fa-lg"></i> Pelican</a></li>
    <li><a href="https://www.python.org/" >
      <i class="fas fa-external-link-square-alt fa-fw fa-lg"></i> Python.org</a></li>
    <li><a href="https://palletsprojects.com/p/jinja/" >
      <i class="fas fa-external-link-square-alt fa-fw fa-lg"></i> Jinja2</a></li>
  </ul>
<h4><i class="fas fa-tags fa-fw fa-lg"></i> Tags</h4>
<p class="tag-cloud">
      <a href="/tag/linux.html">
        <i class="fas fa-tag fa-fw fa-lg"></i>linux
      </a>
      <a href="/tag/os.html">
        <i class="fas fa-tag fa-fw fa-lg"></i>os
      </a>
      <a href="/tag/fedora.html">
        <i class="fas fa-tag fa-fw fa-lg"></i>fedora
      </a>
      <a href="/tag/networking.html">
        <i class="fas fa-tag fa-fw fa-lg"></i>networking
      </a>
      <a href="/tag/wireguard.html">
        <i class="fas fa-tag fa-fw fa-lg"></i>wireguard
      </a>
      <a href="/tag/privacy.html">
        <i class="fas fa-tag fa-fw fa-lg"></i>privacy
      </a>
      <a href="/tag/media.html">
        <i class="fas fa-tag fa-fw fa-lg"></i>media
      </a>
      <a href="/tag/hardware.html">
        <i class="fas fa-tag fa-fw fa-lg"></i>hardware
      </a>
      <a href="/tag/software.html">
        <i class="fas fa-tag fa-fw fa-lg"></i>software
      </a>
      <a href="/tag/smarthome.html">
        <i class="fas fa-tag fa-fw fa-lg"></i>smarthome
      </a>
      <a href="/tag/sustainability.html">
        <i class="fas fa-tag fa-fw fa-lg"></i>sustainability
      </a>
      <a href="/tag/mqtt.html">
        <i class="fas fa-tag fa-fw fa-lg"></i>mqtt
      </a>
      <a href="/tag/photography.html">
        <i class="fas fa-tag fa-fw fa-lg"></i>photography
      </a>
      <a href="/tag/security.html">
        <i class="fas fa-tag fa-fw fa-lg"></i>security
      </a>
      <a href="/tag/mobile.html">
        <i class="fas fa-tag fa-fw fa-lg"></i>mobile
      </a>
      <a href="/tag/quicktip.html">
        <i class="fas fa-tag fa-fw fa-lg"></i>quicktip
      </a>
      <a href="/tag/sun.html">
        <i class="fas fa-tag fa-fw fa-lg"></i>sun
      </a>
      <a href="/tag/arduino.html">
        <i class="fas fa-tag fa-fw fa-lg"></i>arduino
      </a>
      <a href="/tag/web.html">
        <i class="fas fa-tag fa-fw fa-lg"></i>web
      </a>
      <a href="/tag/bookmarks.html">
        <i class="fas fa-tag fa-fw fa-lg"></i>bookmarks
      </a>
      <a href="/tag/documentation.html">
        <i class="fas fa-tag fa-fw fa-lg"></i>documentation
      </a>
      <a href="/tag/blog.html">
        <i class="fas fa-tag fa-fw fa-lg"></i>blog
      </a>
      <a href="/tag/housekeeping.html">
        <i class="fas fa-tag fa-fw fa-lg"></i>housekeeping
      </a>
      <a href="/tag/markdown.html">
        <i class="fas fa-tag fa-fw fa-lg"></i>markdown
      </a>
      <a href="/tag/cloud.html">
        <i class="fas fa-tag fa-fw fa-lg"></i>cloud
      </a>
      <a href="/tag/spyware.html">
        <i class="fas fa-tag fa-fw fa-lg"></i>spyware
      </a>
      <a href="/tag/phone.html">
        <i class="fas fa-tag fa-fw fa-lg"></i>phone
      </a>
      <a href="/tag/applications.html">
        <i class="fas fa-tag fa-fw fa-lg"></i>applications
      </a>
      <a href="/tag/chat.html">
        <i class="fas fa-tag fa-fw fa-lg"></i>chat
      </a>
      <a href="/tag/mastodon.html">
        <i class="fas fa-tag fa-fw fa-lg"></i>mastodon
      </a>
      <a href="/tag/gui.html">
        <i class="fas fa-tag fa-fw fa-lg"></i>gui
      </a>
      <a href="/tag/application.html">
        <i class="fas fa-tag fa-fw fa-lg"></i>application
      </a>
</p>

<hr />

        </div><!--/sidebar -->
      </div><!--/row-->
    </div><!--/.container /#main-container -->

    <footer id="site-footer">
 
      <address id="site-colophon">
        <p class="text-center text-muted">
        Site built using <a href="http://getpelican.com/" target="_blank">Pelican</a>
        &nbsp;&bull;&nbsp; Theme based on
        <a href="http://www.voidynullness.net/page/voidy-bootstrap-pelican-theme/"
           target="_blank">VoidyBootstrap</a> by 
        <a href="http://voidynullness.net"
           target="_blank">RKI</a>  
        </p>
      </address><!-- /colophon  -->
    </footer>


    <!-- javascript -->
   
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js" integrity="sha384-nvAa0+6Qg9clwYCGGPpDQLVpLNn0fRaROjHqs13t4Ggj3Ez50XnGQqc/r8MhnRDZ" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
            integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
            crossorigin="anonymous"></script>


  </body>
</html>