<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta name="description" content="" />
    <meta name="author" content="fedops" />
    <meta name="generator" content="Pelican (VoidyBootstrap theme)" />

    <title>Selfhosted Calendaring - fedops blog</title>

   
        <link rel="stylesheet"
              href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
              integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u"
              crossorigin="anonymous" />

      <link rel="stylesheet"
            href="https://use.fontawesome.com/releases/v5.0.13/css/all.css"
            integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp"
            crossorigin="anonymous"
      />




      <link rel="stylesheet" href="/theme/css/pygment.css" />
      <link rel="stylesheet" href="/theme/css/voidybootstrap.css" />

    <link rel="shortcut icon" href="/favicon.ico" />
  </head>

  <body>
   
    <nav class="navbar navbar-default">
      <div class="container">
	   <div class="navbar-header">
		<button type="button" class="navbar-toggle" 
				data-toggle="collapse" data-target="#main-navbar-collapse">
		  <span class="sr-only">Toggle navigation</span>
		  <span class="icon-bar"></span>
		  <span class="icon-bar"></span>
		  <span class="icon-bar"></span>
		</button>
		<a class="navbar-brand" href="/" rel="home">
          <i class="fas fa-home fa-fw fa-lg"> </i> </a>
       </div>

      <div class="collapse navbar-collapse" id="main-navbar-collapse">
        <ul class="nav navbar-nav">
              <li>
                <a href="/pages/about-this-blog.html">About this Blog</a>
              </li>
            <li class="divider"></li>
            <li class="">
              <a href="/archives.html">Archives</a>
            </li>
          <li class="divider"></li>
        </ul> <!-- /nav -->
      </div> <!-- /navbar-collapse -->
	  </div> <!-- /container -->
    </nav> <!-- /navbar -->

	<div class="jumbotron" id="overview">
	  <div class="container">
		<h1><a href="/">fedops blog</a></h1>
		<p class="lead">Privacy in Computing</p>
	  </div>
	</div>

    <div class="container" id="main-container">
      <div class="row">
        <div class="col-md-9" id="content">
<article itemscope="itemscope" itemtype="http://schema.org/BlogPosting">
  <header class="article-header">
<abbr class="article-header-date">
  Tue 30 August 2022
</abbr> <h1>
  <a href="/selfhosted-calendaring.html" rel="bookmark"
     title="Permalink to Selfhosted Calendaring">
    Selfhosted Calendaring
  </a>
</h1><div class="article-header-info">
  <p>
      Posted by <a href="/author/fedops.html">fedops</a>
    in 
    <a href="/category/software.html">
      Software</a>
    &nbsp;&nbsp;
  </p>
</div> <!-- /.article-header-info -->  </header>
  <div class="content-body" itemprop="text articleBody">
	<p>I've avoided delving into the calendaring topic for a long while. It seemed a
hassle and also I wasn't sure how to best integrate work and private calendars.
I don't have very many private appointments anyway, and the ones I do have
should ideally also block out the respective times in my work calendar so people
don't book me for a meeting when I'm at the dentist or something. So I just put
my private things into the work calendar, with details obfuscated, and that
worked fine.</p>
<p>Syncing appointments or even just accessing them via a private device wasn't
acceptable but I had found <a href="https://www.outlookgooglecalendarsync.com/">Outlook - Google Calendar
Sync</a> which worked from my work
laptop but only to and from a Google calendar. So when I tossed that out I was
left without a working sync. With an upcoming job change and no idea what they
will be doing for calendering I finally decided to set up a private solution.</p>
<h1 id="software">Software</h1>
<p>There are essentially 3 parts to it. First, a server side that maintains one or
more calendars and serves as the "source of truth" for the contents.</p>
<p>Second, a way to synchronize multiple parties into and out of it, like my
primary laptop, the smartphone, and eventually also the work calendar. All of
these should be two-way syncs with the server holding the sum of everything.</p>
<p>And third, calendar applications on all devices. There are lots of choices on the
phone and I didn't want to be tied into any specific one. Likewise on the
desktop. So I really wanted a separate sync client and calendar frontend if
possible.</p>
<p>Here's an overview of the concept:</p>
<p><img alt="overview" src="/images/calendaring.png"></p>
<h2 id="server">Server</h2>
<p>After a bit of research and looking into <a href="https://sabre.io/baikal/">Baïkal</a>
(nice UI, but requires a web server with PHP to run) and
<a href="https://www.xandikos.org/">Xandikos</a> (written in python, but requires uWSGI to
run) I decided to give <a href="https://radicale.org/">radicale</a> a try<sup id="fnref:1"><a class="footnote-ref" href="#fn:1">1</a></sup>. The feature list
looked very promising:</p>
<ul>
<li>Shares calendars and contact lists through CalDAV, CardDAV and HTTP.</li>
<li>Supports events, todos, journal entries and business cards.</li>
<li>Works out-of-the-box, no complicated setup or configuration required.</li>
<li>Can limit access by authentication.</li>
<li>Can secure connections with TLS.</li>
<li>Works with many CalDAV and CardDAV clients.</li>
<li>Stores all data on the file system in a simple folder structure.</li>
<li>Can be extended with plugins.</li>
<li>Is GPLv3-licensed free software.</li>
</ul>
<p>Installation was straightforward using <code>pip3</code> and the "Simple 5-minute Setup" in
the excellent documentation was all that was needed to get up and running. My
VPS is still running CentOS 7, the <code>systemd</code> of which does not support Dbus
access for user units, so I ended up following the systemwide setup suggestions
and it works just fine. The only negative thing is individual users do not have
raw file system access to their calenders without some manual fixes, but that's
not a big deal.</p>
<p>Everything works exactly as it says on the tin. I specifically like that the
calendar files can be checked into a Git repo automatically so that's the backup
and version control taken care of right away. Nice!</p>
<p>As usual the calendar server's port is only opened on the Wireguard tunnel
interface so access from non-VPN devices is impossible, as it should be.</p>
<h2 id="synchronization">Synchronization</h2>
<p>Being standards-compliant there is a plethora of syncing software that can be
used.</p>
<p>On the phone I'm using
<a href="https://f-droid.org/en/packages/at.bitfire.davdroid/">DAVx5</a> from F-Droid which
presents the calendars to the Android system, and then every calendar app can be
used as the frontend.</p>
<p>On the Linux desktop side
<a href="https://vdirsyncer.pimutils.org/en/stable/index.html">vdirsyncer</a> is a very
customizable middleware which works great with radicale and will hopefully also
sync with work's setup, whatever that may turn out to be. Update later this
year.</p>
<p>Vdirsyncer stores the .ics files in a local directory structure so there
are enough possibilities to make use of that in scripts without having to go
through the complexities of talking to a CalDAV server.</p>
<h2 id="clients">Clients</h2>
<p>On the phone one option is <a href="https://f-droid.org/packages/ws.xsoh.etar/">Etar</a>
but as mentioned there's another new one every month to try out. There is little
risk of losing anything while "calendar-hopping" as the appointments are not
stored in the app itself.</p>
<p>For Linux I had already found <a href="https://khal.readthedocs.io/en/latest/index.html">khal</a>
from pimutils a while ago which looked very nice. I like that it has a mode to
display a calendar exactly like <code>cal</code> does, just with your appointments added in
a column on the right.</p>
<p>More normal people would probably use Thunderbird or other GUI apps, but again -
as long as it speaks CalDAV it'll probably work.</p>
<h1 id="loose-ends">Loose Ends</h1>
<p>One topic I haven't yet looked into is contacts handling. I understand it should
be possible to use CardDAV in a similar fashion to CalDAV for calendaring, but
for it to be worthwhile I need to find a way to sync with my Alpine address book
also. That will probably be a bespoke script or maybe
<a href="https://github.com/lucc/khard">khard</a> could be used, we'll see. I'll update
this article when I get to it.</p>
<div class="footnote">
<hr>
<ol>
<li id="fn:1">
<p>not implying those two or indeed others aren't at least as good, it's just
what I looked at and liked. radicale does have an extremely limited UI which I
don't need but which might be considered a downside by others.&#160;<a class="footnote-backref" href="#fnref:1" title="Jump back to footnote 1 in the text">&#8617;</a></p>
</li>
</ol>
</div>
  </div>
  
<div class="article-tag-list">
<span class="label label-default">Tags</span>
	<a href="/tag/linux.html"><i class="fas fa-tag"></i>linux</a>&nbsp;
	<a href="/tag/software.html"><i class="fas fa-tag"></i>software</a>&nbsp;
	<a href="/tag/privacy.html"><i class="fas fa-tag"></i>privacy</a>&nbsp;
</div><!-- via neighbor plugin, see: https://github.com/pelican-plugins/neighbors -->
    <hr />
    <p class="content-emphasis">
	<table width="100%">
	    <tr>
			<td align="left">
	        </td>
	        <td align="right">
	        </td>
		</tr>
	</table>
	</p>
</article>
        </div><!-- /content -->

        <div class="col-md-3 sidebar-nav" id="sidebar">

<div class="row">

<div class="col-xs-6 col-md-12">
<h4><i class="fas fa-comment fa-fw fa-lg"></i> Social</h4>
<ul class="list-unstyled social-links">
    <li><a href="https://fosstodon.org/@fedops" target="_blank">
	  <i class="fas fa-comments fa-fw fa-lg" title="Fosstodon"></i>
		Fosstodon
	</a></li>
    <li><a href="https://codeberg.org/fedops" target="_blank">
	  <i class="fas fa-comments fa-fw fa-lg" title="Codeberg"></i>
		Codeberg
	</a></li>
</ul>
</div>

<div class="col-xs-6 col-md-12">
<h4><i class="fas fa-folder fa-fw fa-lg"></i> Categories</h4>
<ul class="list-unstyled category-links">
  <li><a href="/category/cloud.html" >
    <i class="fas fa-folder-open fa-fw fa-lg"></i> Cloud</a></li>
  <li><a href="/category/hardware.html" >
    <i class="fas fa-folder-open fa-fw fa-lg"></i> Hardware</a></li>
  <li><a href="/category/howto.html" >
    <i class="fas fa-folder-open fa-fw fa-lg"></i> Howto</a></li>
  <li><a href="/category/infomanagement.html" >
    <i class="fas fa-folder-open fa-fw fa-lg"></i> InfoManagement</a></li>
  <li><a href="/category/media.html" >
    <i class="fas fa-folder-open fa-fw fa-lg"></i> Media</a></li>
  <li><a href="/category/misc.html" >
    <i class="fas fa-folder-open fa-fw fa-lg"></i> misc</a></li>
  <li><a href="/category/phone.html" >
    <i class="fas fa-folder-open fa-fw fa-lg"></i> Phone</a></li>
  <li><a href="/category/privacy.html" >
    <i class="fas fa-folder-open fa-fw fa-lg"></i> Privacy</a></li>
  <li><a href="/category/security.html" >
    <i class="fas fa-folder-open fa-fw fa-lg"></i> Security</a></li>
  <li><a href="/category/software.html" >
    <i class="fas fa-folder-open fa-fw fa-lg"></i> Software</a></li>
</ul>
</div>

</div> <!-- /row -->

  <h4><i class="fas fa-link fa-fw fa-lg"></i> Links</h4>
  <ul class="list-unstyled category-links">
    <li><a href="https://getpelican.com/" >
      <i class="fas fa-external-link-square-alt fa-fw fa-lg"></i> Pelican</a></li>
    <li><a href="https://www.python.org/" >
      <i class="fas fa-external-link-square-alt fa-fw fa-lg"></i> Python.org</a></li>
    <li><a href="https://palletsprojects.com/p/jinja/" >
      <i class="fas fa-external-link-square-alt fa-fw fa-lg"></i> Jinja2</a></li>
  </ul>
<h4><i class="fas fa-tags fa-fw fa-lg"></i> Tags</h4>
<p class="tag-cloud">
      <a href="/tag/linux.html">
        <i class="fas fa-tag fa-fw fa-lg"></i>linux
      </a>
      <a href="/tag/os.html">
        <i class="fas fa-tag fa-fw fa-lg"></i>os
      </a>
      <a href="/tag/fedora.html">
        <i class="fas fa-tag fa-fw fa-lg"></i>fedora
      </a>
      <a href="/tag/networking.html">
        <i class="fas fa-tag fa-fw fa-lg"></i>networking
      </a>
      <a href="/tag/wireguard.html">
        <i class="fas fa-tag fa-fw fa-lg"></i>wireguard
      </a>
      <a href="/tag/privacy.html">
        <i class="fas fa-tag fa-fw fa-lg"></i>privacy
      </a>
      <a href="/tag/media.html">
        <i class="fas fa-tag fa-fw fa-lg"></i>media
      </a>
      <a href="/tag/hardware.html">
        <i class="fas fa-tag fa-fw fa-lg"></i>hardware
      </a>
      <a href="/tag/software.html">
        <i class="fas fa-tag fa-fw fa-lg"></i>software
      </a>
      <a href="/tag/smarthome.html">
        <i class="fas fa-tag fa-fw fa-lg"></i>smarthome
      </a>
      <a href="/tag/sustainability.html">
        <i class="fas fa-tag fa-fw fa-lg"></i>sustainability
      </a>
      <a href="/tag/mqtt.html">
        <i class="fas fa-tag fa-fw fa-lg"></i>mqtt
      </a>
      <a href="/tag/photography.html">
        <i class="fas fa-tag fa-fw fa-lg"></i>photography
      </a>
      <a href="/tag/security.html">
        <i class="fas fa-tag fa-fw fa-lg"></i>security
      </a>
      <a href="/tag/mobile.html">
        <i class="fas fa-tag fa-fw fa-lg"></i>mobile
      </a>
      <a href="/tag/quicktip.html">
        <i class="fas fa-tag fa-fw fa-lg"></i>quicktip
      </a>
      <a href="/tag/sun.html">
        <i class="fas fa-tag fa-fw fa-lg"></i>sun
      </a>
      <a href="/tag/arduino.html">
        <i class="fas fa-tag fa-fw fa-lg"></i>arduino
      </a>
      <a href="/tag/web.html">
        <i class="fas fa-tag fa-fw fa-lg"></i>web
      </a>
      <a href="/tag/bookmarks.html">
        <i class="fas fa-tag fa-fw fa-lg"></i>bookmarks
      </a>
      <a href="/tag/documentation.html">
        <i class="fas fa-tag fa-fw fa-lg"></i>documentation
      </a>
      <a href="/tag/blog.html">
        <i class="fas fa-tag fa-fw fa-lg"></i>blog
      </a>
      <a href="/tag/housekeeping.html">
        <i class="fas fa-tag fa-fw fa-lg"></i>housekeeping
      </a>
      <a href="/tag/markdown.html">
        <i class="fas fa-tag fa-fw fa-lg"></i>markdown
      </a>
      <a href="/tag/cloud.html">
        <i class="fas fa-tag fa-fw fa-lg"></i>cloud
      </a>
      <a href="/tag/spyware.html">
        <i class="fas fa-tag fa-fw fa-lg"></i>spyware
      </a>
      <a href="/tag/phone.html">
        <i class="fas fa-tag fa-fw fa-lg"></i>phone
      </a>
      <a href="/tag/applications.html">
        <i class="fas fa-tag fa-fw fa-lg"></i>applications
      </a>
      <a href="/tag/chat.html">
        <i class="fas fa-tag fa-fw fa-lg"></i>chat
      </a>
      <a href="/tag/mastodon.html">
        <i class="fas fa-tag fa-fw fa-lg"></i>mastodon
      </a>
      <a href="/tag/gui.html">
        <i class="fas fa-tag fa-fw fa-lg"></i>gui
      </a>
      <a href="/tag/application.html">
        <i class="fas fa-tag fa-fw fa-lg"></i>application
      </a>
</p>

<hr />

        </div><!--/sidebar -->
      </div><!--/row-->
    </div><!--/.container /#main-container -->

    <footer id="site-footer">
 
      <address id="site-colophon">
        <p class="text-center text-muted">
        Site built using <a href="http://getpelican.com/" target="_blank">Pelican</a>
        &nbsp;&bull;&nbsp; Theme based on
        <a href="http://www.voidynullness.net/page/voidy-bootstrap-pelican-theme/"
           target="_blank">VoidyBootstrap</a> by 
        <a href="http://voidynullness.net"
           target="_blank">RKI</a>  
        </p>
      </address><!-- /colophon  -->
    </footer>


    <!-- javascript -->
   
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js" integrity="sha384-nvAa0+6Qg9clwYCGGPpDQLVpLNn0fRaROjHqs13t4Ggj3Ez50XnGQqc/r8MhnRDZ" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
            integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
            crossorigin="anonymous"></script>


  </body>
</html>