<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta name="description" content="" />
    <meta name="author" content="fedops" />
    <meta name="generator" content="Pelican (VoidyBootstrap theme)" />

    <title>The Case for Per-Service Email Addresses - fedops blog</title>

   
        <link rel="stylesheet"
              href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
              integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u"
              crossorigin="anonymous" />

      <link rel="stylesheet"
            href="https://use.fontawesome.com/releases/v5.0.13/css/all.css"
            integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp"
            crossorigin="anonymous"
      />




      <link rel="stylesheet" href="/theme/css/pygment.css" />
      <link rel="stylesheet" href="/theme/css/voidybootstrap.css" />

    <link rel="shortcut icon" href="/favicon.ico" />
  </head>

  <body>
   
    <nav class="navbar navbar-default">
      <div class="container">
	   <div class="navbar-header">
		<button type="button" class="navbar-toggle" 
				data-toggle="collapse" data-target="#main-navbar-collapse">
		  <span class="sr-only">Toggle navigation</span>
		  <span class="icon-bar"></span>
		  <span class="icon-bar"></span>
		  <span class="icon-bar"></span>
		</button>
		<a class="navbar-brand" href="/" rel="home">
          <i class="fas fa-home fa-fw fa-lg"> </i> </a>
       </div>

      <div class="collapse navbar-collapse" id="main-navbar-collapse">
        <ul class="nav navbar-nav">
              <li>
                <a href="/pages/about-this-blog.html">About this Blog</a>
              </li>
            <li class="divider"></li>
            <li class="">
              <a href="/archives.html">Archives</a>
            </li>
          <li class="divider"></li>
        </ul> <!-- /nav -->
      </div> <!-- /navbar-collapse -->
	  </div> <!-- /container -->
    </nav> <!-- /navbar -->

	<div class="jumbotron" id="overview">
	  <div class="container">
		<h1><a href="/">fedops blog</a></h1>
		<p class="lead">Privacy in Computing</p>
	  </div>
	</div>

    <div class="container" id="main-container">
      <div class="row">
        <div class="col-md-9" id="content">
<article itemscope="itemscope" itemtype="http://schema.org/BlogPosting">
  <header class="article-header">
<abbr class="article-header-date">
  Wed 25 May 2022
</abbr> <h1>
  <a href="/the-case-for-per-service-email-addresses.html" rel="bookmark"
     title="Permalink to The Case for Per-Service Email Addresses">
    The Case for Per-Service Email Addresses
  </a>
</h1><div class="article-header-info">
  <p>
      Posted by <a href="/author/fedops.html">fedops</a>
    in 
    <a href="/category/security.html">
      Security</a>
    &nbsp;&nbsp;
  </p>
</div> <!-- /.article-header-info -->  </header>
  <div class="content-body" itemprop="text articleBody">
	<p>Phishing, online scams, and identity theft are big problems. All three of them are
quite closely related, and to a significant degree revolve around something
virtually everyone has these days - an email address. Let's look at the
definitions real quick:</p>
<p><strong>Phishing</strong> is a type of social engineering where an attacker sends a
fraudulent (deceptive) message designed to trick a person into revealing
sensitive information to the attacker.</p>
<p>A <strong>scam</strong> is a deceptive scheme or trick used to cheat someone out of
something, especially money. </p>
<div class="toc"><span class="toctitle">Table of Contents</span><ul>
<li><a href="#not-your-grannys-spam-anymore">Not Your Granny's Spam Anymore</a></li>
<li><a href="#why-is-this-a-thing">Why Is This A Thing?</a></li>
<li><a href="#where-do-the-addresses-come-from">Where Do The Addresses Come From?</a></li>
<li><a href="#what-to-do">What To Do?</a></li>
<li><a href="#isnt-this-hard">Isn't This Hard?</a></li>
<li><a href="#is-this-a-replacement-for-per-service-passwords">Is This a Replacement for Per-Service Passwords?</a></li>
<li><a href="#what-if-an-address-is-compromised">What If An Address Is Compromised?</a></li>
<li><a href="#is-this-practical">Is This Practical?</a></li>
</ul>
</div>
<p><strong>Identity theft</strong> is the crime of obtaining the personal or financial
information of another person to use their identity to commit fraud, such as
making unauthorized transactions or purchases.</p>
<h1 id="not-your-grannys-spam-anymore">Not Your Granny's Spam Anymore</h1>
<p>In general these activities are performed as organized crime; i.e. an actual
company specializes in crafting attacks and deploying them against tens,
sometimes hundreds of thousands of victims, in waves. Even a relatively low
success rate of maybe 1 in 1000 will still result in a positive business case.</p>
<p>Everyone has received what we like to call spam emails. These used to be "call
this number for cheap Viagra" type mass mailings which were easily identified
and deleted. Not any more. Well, they still roll in but more concerningly
nowadays you also receive a well-prepared email which appears to be from your actual
bank asking you to download a bank statement by clicking on a link. Or a message
from the insurance advising you they made an accounting error for which you can
claim your money back by reviewing the attached PDF. Or the tax advisor asking
you to confirm their declaration. Etc.</p>
<p>They have well-formatted letterheads, use wording consistent with their claimed
origin, have legal disclaimers attached, ... In other words, at first (and
sometimes, second and third) glance they look legit. And usually they try to
rush you into immediate action, almost invariably because money appears to be at risk.</p>
<h1 id="why-is-this-a-thing">Why Is This A Thing?</h1>
<p>The main reason such large-scale waves can be efficiently executed is because
"harvested" email addresses are bought and sold on the black market, for pennies
apiece. If an organization buys 100,000 email addresses for 1,000 Euros,
launches a wave, and manages to steal several thousand Euros from just a few
victims, they come out making a profit.</p>
<p>The underlying problem is that email addresses are used everywhere, can be
easily obtained, and can be used to very cost-efficiently deliver such attack
waves, at zero risk to the attackers.</p>
<h1 id="where-do-the-addresses-come-from">Where Do The Addresses Come From?</h1>
<p>Preventing having your email address being stolen is surprisingly hard. For
example, you may have registered it at Amazon, and you purchase something from a
marketplace vendor. Amazon automatically submits your order, including your
email address, into the vendor's IT system for them to process the order and
ship your product. This system may be infected with malware, or may just be
operated by shady characters, and your email address is siphoned off of it; in
other words, harvested. It will actually be a comparatively valuable (i.e.,
expensive on the dark web) address because it is <em>verified</em> -- you've been using
it to log on to Amazon just a minute ago and you are receiving emails there.
All this happened without your knowledge, on a computer somewhere
owned by a company you've never heard of.</p>
<p>Other avenues include mailing list and website registrations. Everywhere you go
on the web these days, sites will ask you to register or sign up for some petty
benefits, like daily newsletters, coupon codes, rebates, or even just to tweak
settings. The usual way to do that is by providing an email address. And of
course if something is "free", usually you are the product. Some sites will
outright sell that information, but in many cases their security is lacking
and sooner or later a hack will lead to the address database being
copied/leaked/stolen.</p>
<p>The same goes for your brick &amp; mortar tire dealer or the motel you stayed at
that ask for your email address to send you the invoice, etc. Want to bet their
system still runs Windows XP, has no virus scanner, and is riddled with
questionable software?</p>
<h1 id="what-to-do">What To Do?</h1>
<p>Ok so it's hard to prevent email addresses going places. What can you do? Two things:</p>
<ol>
<li>be mindful what you sign up to and err on the side of caution<sup id="fnref:pd"><a class="footnote-ref" href="#fn:pd">1</a></sup></li>
<li>use per-service email addresses</li>
</ol>
<p>This will not prevent getting hit with phishing or scam emails, but it will make
it easier to identify them as such. Here's how.</p>
<p>Let's say you register at that motel because you want to get the reduced
members' rate. To register you use the address <code>&lt;fedops-motel@your.domain&gt;</code>.
From now on that motel chain's communication will reach you at that address, and
that address only.</p>
<p>Next, you need to give your bank an email address to reach you, and you use
<code>&lt;fedops-bank@your.domain&gt;</code>. Same deal, bank emails will now go to that address.
None of these addresses will get used anywhere else - for every company and 
every service you come up with a new address.</p>
<p>Now, if that motel's Windows XP machine gets popped and your and thousands of
other email addresses end up being hit with a scam wave, all of a sudden you
will receive an email which purports to be from your bank (or your insurance or
your car leasing firm). But it will be addressed to
<code>&lt;fedops-motel@your.domain&gt;</code>, because that's the addresses that got stolen. You
have never given that address to your bank (or insurance). So with just one look
you can identify this mail as being fake, no matter how legit it looks, and
delete it. Case closed. Plus, you can even tell which entity lost your address.</p>
<h1 id="isnt-this-hard">Isn't This Hard?</h1>
<p>There's clearly additional effort on your part:</p>
<ul>
<li>You need to come up with addresses per-service as you go.</li>
<li>I strongly recommend you keep track of the addresses you used so you know
  who's the perpetrator and also to prevent you from accidentally using the same
  address multiple times. A good way to do that is to put the address into your
  password manager where you also store the password and any other information
  related to that account, such as your motel membership number.</li>
<li>You need some way to register all those addresses and to somehow collect them
  into your mailbox.</li>
</ul>
<p>The last part is the most difficult one. You don't want to deal with dozens of
mailboxes, so you need some service to collect them and forward them to your
actual mailbox.</p>
<p>If you self-host your email you have that covered. Either you create email
aliases on your server, or you use what's called a catch-all address. Following
the pattern above everything after the "-" is stripped off and the mail is
forwarded to the "fedops" mailbox on your server.</p>
<p>If you use hosted email (as 90+% of the population do nowadays) you can either
use a virtual address service that your mail provider offers (check their
services offerings), or you can use sites such as Namecheap or ForwardEmail
which provide that independent of where your actual email lives. Search for "email
forwarders" to find other companies or organizations providing such services.</p>
<p>Be mindful who you use though, because if that service closes up shop you will
suddenly be cut off from receiving mails at any of those addresses. It may be
worth paying a reputable provider which hosts both your email /and/ provides
that service. And, please, get off of Gmail and Outlook.com while you're at it.</p>
<h1 id="is-this-a-replacement-for-per-service-passwords">Is This a Replacement for Per-Service Passwords?</h1>
<p>Absolutely not! You should <strong>absolutely</strong> use distinct passwords for your online
accounts, and you should maintain this information in your password manager.</p>
<p>But having per-service email addresses further reduces the attack surface,
especially in brute-force attacks. If someone obtains the user list of a service
they hacked into, utilizing this list in an attack against another service will
not help them to gain access to your account since it's not the same address
(i.e., user name).</p>
<h1 id="what-if-an-address-is-compromised">What If An Address Is Compromised?</h1>
<p>So you're receiving mails at one of your addresses, and it's driving you nuts.
Nigerian spam kings, Viagra, fake bank statements, they just keep rolling
in on that motel address.</p>
<p>The best way forward is to go to the motel's site, log in with your account
credentials (which you have saved in your password manager, right?), and change
your email address. You retire <code>&lt;fedops-motel@your.domain&gt;</code> and change it to
<code>&lt;fedops-motelnew@your.domain</code>&gt;. Only the actual motel emails will go to that new
address, the spam continues to arrive at the old one. And this one you simply
block in your forwarding setup, or alternatively filter out in your mail
software to go to trash immediately. None of your other email addresses are
affected by this change.</p>
<h1 id="is-this-practical">Is This Practical?</h1>
<p>I think it is. I've been using this system for about 20 years with a current total of
just over 300 email addresses. In that time I have retired about 20 addresses
due to unwanted emails, some of them even multiple times.</p>
<p>The most problematic ones have been my (long retired) Adobe account (they have had multiple
episodes of parts of their customer databases being stolen<sup id="fnref:ad1"><a class="footnote-ref" href="#fn:ad1">2</a></sup> <sup id="fnref:ad2"><a class="footnote-ref" href="#fn:ad2">3</a></sup>), EBay and Amazon
(3rd party vendors with no opsec on their systems), Paypal (same issue, I don't think
Paypal themselves ever got hacked), and 2 web forum registrations where the
phpBB installations got hacked<sup id="fnref:phpbb"><a class="footnote-ref" href="#fn:phpbb">4</a></sup>.</p>
<p>Oh, and the one I used for South African National Parks which I knew was a goner
when upon check-in at a rest camp I saw the PC was so full of malware it had
basically slowed to a crawl. In addition to the email address I also got my
credit card information stolen. That was in 2008, and to this day I'm still
getting 1-2 spam mails per day to that address. It would take SANParks another 9
years to finally implement security standards to become PCI DSS-compliant<sup id="fnref:san"><a class="footnote-ref" href="#fn:san">5</a></sup>.</p>
<p>So yeah - it's some additional work but it's worth it.</p>
<div class="footnote">
<hr>
<ol>
<li id="fn:pd">
<p>And also, provide as few personal details as possible. That motel
doesn't need to know your real birth date or marital status. Just fake
something. Identity theft is everywhere.&#160;<a class="footnote-backref" href="#fnref:pd" title="Jump back to footnote 1 in the text">&#8617;</a></p>
</li>
<li id="fn:ad1">
<p><a href="https://www.zdnet.com/article/adobe-left-7-5-million-creative-cloud-user-records-exposed-online/">https://www.zdnet.com/article/adobe-left-7-5-million-creative-cloud-user-records-exposed-online/</a>&#160;<a class="footnote-backref" href="#fnref:ad1" title="Jump back to footnote 2 in the text">&#8617;</a></p>
</li>
<li id="fn:ad2">
<p>I was in this one: <a href="https://www.bbc.com/news/technology-24740873">https://www.bbc.com/news/technology-24740873</a>&#160;<a class="footnote-backref" href="#fnref:ad2" title="Jump back to footnote 3 in the text">&#8617;</a></p>
</li>
<li id="fn:phpbb">
<p>phpBB is just a complete security trainwreck:
<a href="https://www.cvedetails.com/vulnerability-list/vendor_id-1529/Phpbb.html">https://www.cvedetails.com/vulnerability-list/vendor_id-1529/Phpbb.html</a>&#160;<a class="footnote-backref" href="#fnref:phpbb" title="Jump back to footnote 4 in the text">&#8617;</a></p>
</li>
<li id="fn:san">
<p><a href="https://www.sanparks.org/about/news/?id=57279">https://www.sanparks.org/about/news/?id=57279</a>&#160;<a class="footnote-backref" href="#fnref:san" title="Jump back to footnote 5 in the text">&#8617;</a></p>
</li>
</ol>
</div>
  </div>
  
<div class="article-tag-list">
<span class="label label-default">Tags</span>
	<a href="/tag/security.html"><i class="fas fa-tag"></i>security</a>&nbsp;
	<a href="/tag/privacy.html"><i class="fas fa-tag"></i>privacy</a>&nbsp;
</div><!-- via neighbor plugin, see: https://github.com/pelican-plugins/neighbors -->
    <hr />
    <p class="content-emphasis">
	<table width="100%">
	    <tr>
			<td align="left">
	        </td>
	        <td align="right">
	        </td>
		</tr>
	</table>
	</p>
</article>
        </div><!-- /content -->

        <div class="col-md-3 sidebar-nav" id="sidebar">

<div class="row">

<div class="col-xs-6 col-md-12">
<h4><i class="fas fa-comment fa-fw fa-lg"></i> Social</h4>
<ul class="list-unstyled social-links">
    <li><a href="https://fosstodon.org/@fedops" target="_blank">
	  <i class="fas fa-comments fa-fw fa-lg" title="Fosstodon"></i>
		Fosstodon
	</a></li>
    <li><a href="https://codeberg.org/fedops" target="_blank">
	  <i class="fas fa-comments fa-fw fa-lg" title="Codeberg"></i>
		Codeberg
	</a></li>
</ul>
</div>

<div class="col-xs-6 col-md-12">
<h4><i class="fas fa-folder fa-fw fa-lg"></i> Categories</h4>
<ul class="list-unstyled category-links">
  <li><a href="/category/cloud.html" >
    <i class="fas fa-folder-open fa-fw fa-lg"></i> Cloud</a></li>
  <li><a href="/category/hardware.html" >
    <i class="fas fa-folder-open fa-fw fa-lg"></i> Hardware</a></li>
  <li><a href="/category/howto.html" >
    <i class="fas fa-folder-open fa-fw fa-lg"></i> Howto</a></li>
  <li><a href="/category/infomanagement.html" >
    <i class="fas fa-folder-open fa-fw fa-lg"></i> InfoManagement</a></li>
  <li><a href="/category/media.html" >
    <i class="fas fa-folder-open fa-fw fa-lg"></i> Media</a></li>
  <li><a href="/category/misc.html" >
    <i class="fas fa-folder-open fa-fw fa-lg"></i> misc</a></li>
  <li><a href="/category/phone.html" >
    <i class="fas fa-folder-open fa-fw fa-lg"></i> Phone</a></li>
  <li><a href="/category/privacy.html" >
    <i class="fas fa-folder-open fa-fw fa-lg"></i> Privacy</a></li>
  <li><a href="/category/security.html" >
    <i class="fas fa-folder-open fa-fw fa-lg"></i> Security</a></li>
  <li><a href="/category/software.html" >
    <i class="fas fa-folder-open fa-fw fa-lg"></i> Software</a></li>
</ul>
</div>

</div> <!-- /row -->

  <h4><i class="fas fa-link fa-fw fa-lg"></i> Links</h4>
  <ul class="list-unstyled category-links">
    <li><a href="https://getpelican.com/" >
      <i class="fas fa-external-link-square-alt fa-fw fa-lg"></i> Pelican</a></li>
    <li><a href="https://www.python.org/" >
      <i class="fas fa-external-link-square-alt fa-fw fa-lg"></i> Python.org</a></li>
    <li><a href="https://palletsprojects.com/p/jinja/" >
      <i class="fas fa-external-link-square-alt fa-fw fa-lg"></i> Jinja2</a></li>
  </ul>
<h4><i class="fas fa-tags fa-fw fa-lg"></i> Tags</h4>
<p class="tag-cloud">
      <a href="/tag/linux.html">
        <i class="fas fa-tag fa-fw fa-lg"></i>linux
      </a>
      <a href="/tag/os.html">
        <i class="fas fa-tag fa-fw fa-lg"></i>os
      </a>
      <a href="/tag/fedora.html">
        <i class="fas fa-tag fa-fw fa-lg"></i>fedora
      </a>
      <a href="/tag/networking.html">
        <i class="fas fa-tag fa-fw fa-lg"></i>networking
      </a>
      <a href="/tag/wireguard.html">
        <i class="fas fa-tag fa-fw fa-lg"></i>wireguard
      </a>
      <a href="/tag/privacy.html">
        <i class="fas fa-tag fa-fw fa-lg"></i>privacy
      </a>
      <a href="/tag/media.html">
        <i class="fas fa-tag fa-fw fa-lg"></i>media
      </a>
      <a href="/tag/hardware.html">
        <i class="fas fa-tag fa-fw fa-lg"></i>hardware
      </a>
      <a href="/tag/software.html">
        <i class="fas fa-tag fa-fw fa-lg"></i>software
      </a>
      <a href="/tag/smarthome.html">
        <i class="fas fa-tag fa-fw fa-lg"></i>smarthome
      </a>
      <a href="/tag/sustainability.html">
        <i class="fas fa-tag fa-fw fa-lg"></i>sustainability
      </a>
      <a href="/tag/mqtt.html">
        <i class="fas fa-tag fa-fw fa-lg"></i>mqtt
      </a>
      <a href="/tag/photography.html">
        <i class="fas fa-tag fa-fw fa-lg"></i>photography
      </a>
      <a href="/tag/security.html">
        <i class="fas fa-tag fa-fw fa-lg"></i>security
      </a>
      <a href="/tag/mobile.html">
        <i class="fas fa-tag fa-fw fa-lg"></i>mobile
      </a>
      <a href="/tag/quicktip.html">
        <i class="fas fa-tag fa-fw fa-lg"></i>quicktip
      </a>
      <a href="/tag/sun.html">
        <i class="fas fa-tag fa-fw fa-lg"></i>sun
      </a>
      <a href="/tag/arduino.html">
        <i class="fas fa-tag fa-fw fa-lg"></i>arduino
      </a>
      <a href="/tag/web.html">
        <i class="fas fa-tag fa-fw fa-lg"></i>web
      </a>
      <a href="/tag/bookmarks.html">
        <i class="fas fa-tag fa-fw fa-lg"></i>bookmarks
      </a>
      <a href="/tag/documentation.html">
        <i class="fas fa-tag fa-fw fa-lg"></i>documentation
      </a>
      <a href="/tag/blog.html">
        <i class="fas fa-tag fa-fw fa-lg"></i>blog
      </a>
      <a href="/tag/housekeeping.html">
        <i class="fas fa-tag fa-fw fa-lg"></i>housekeeping
      </a>
      <a href="/tag/markdown.html">
        <i class="fas fa-tag fa-fw fa-lg"></i>markdown
      </a>
      <a href="/tag/cloud.html">
        <i class="fas fa-tag fa-fw fa-lg"></i>cloud
      </a>
      <a href="/tag/spyware.html">
        <i class="fas fa-tag fa-fw fa-lg"></i>spyware
      </a>
      <a href="/tag/phone.html">
        <i class="fas fa-tag fa-fw fa-lg"></i>phone
      </a>
      <a href="/tag/applications.html">
        <i class="fas fa-tag fa-fw fa-lg"></i>applications
      </a>
      <a href="/tag/chat.html">
        <i class="fas fa-tag fa-fw fa-lg"></i>chat
      </a>
      <a href="/tag/mastodon.html">
        <i class="fas fa-tag fa-fw fa-lg"></i>mastodon
      </a>
      <a href="/tag/gui.html">
        <i class="fas fa-tag fa-fw fa-lg"></i>gui
      </a>
      <a href="/tag/application.html">
        <i class="fas fa-tag fa-fw fa-lg"></i>application
      </a>
</p>

<hr />

        </div><!--/sidebar -->
      </div><!--/row-->
    </div><!--/.container /#main-container -->

    <footer id="site-footer">
 
      <address id="site-colophon">
        <p class="text-center text-muted">
        Site built using <a href="http://getpelican.com/" target="_blank">Pelican</a>
        &nbsp;&bull;&nbsp; Theme based on
        <a href="http://www.voidynullness.net/page/voidy-bootstrap-pelican-theme/"
           target="_blank">VoidyBootstrap</a> by 
        <a href="http://voidynullness.net"
           target="_blank">RKI</a>  
        </p>
      </address><!-- /colophon  -->
    </footer>


    <!-- javascript -->
   
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js" integrity="sha384-nvAa0+6Qg9clwYCGGPpDQLVpLNn0fRaROjHqs13t4Ggj3Ez50XnGQqc/r8MhnRDZ" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
            integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
            crossorigin="anonymous"></script>


  </body>
</html>