<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta name="description" content="" />
    <meta name="author" content="fedops" />
    <meta name="generator" content="Pelican (VoidyBootstrap theme)" />

    <title>Behind the Scenes of this Blog - fedops blog</title>

   
        <link rel="stylesheet"
              href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
              integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u"
              crossorigin="anonymous" />

      <link rel="stylesheet"
            href="https://use.fontawesome.com/releases/v5.0.13/css/all.css"
            integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp"
            crossorigin="anonymous"
      />




      <link rel="stylesheet" href="/theme/css/pygment.css" />
      <link rel="stylesheet" href="/theme/css/voidybootstrap.css" />

    <link rel="shortcut icon" href="/favicon.ico" />
  </head>

  <body>
   
    <nav class="navbar navbar-default">
      <div class="container">
	   <div class="navbar-header">
		<button type="button" class="navbar-toggle" 
				data-toggle="collapse" data-target="#main-navbar-collapse">
		  <span class="sr-only">Toggle navigation</span>
		  <span class="icon-bar"></span>
		  <span class="icon-bar"></span>
		  <span class="icon-bar"></span>
		</button>
		<a class="navbar-brand" href="/" rel="home">
          <i class="fas fa-home fa-fw fa-lg"> </i> </a>
       </div>

      <div class="collapse navbar-collapse" id="main-navbar-collapse">
        <ul class="nav navbar-nav">
              <li>
                <a href="/pages/about-this-blog.html">About this Blog</a>
              </li>
            <li class="divider"></li>
            <li class="">
              <a href="/archives.html">Archives</a>
            </li>
          <li class="divider"></li>
        </ul> <!-- /nav -->
      </div> <!-- /navbar-collapse -->
	  </div> <!-- /container -->
    </nav> <!-- /navbar -->

	<div class="jumbotron" id="overview">
	  <div class="container">
		<h1><a href="/">fedops blog</a></h1>
		<p class="lead">Privacy in Computing</p>
	  </div>
	</div>

    <div class="container" id="main-container">
      <div class="row">
        <div class="col-md-9" id="content">
<article itemscope="itemscope" itemtype="http://schema.org/BlogPosting">
  <header class="article-header">
<abbr class="article-header-date">
  Wed 06 April 2022
</abbr> <h1>
  <a href="/behind-the-scenes-of-this-blog.html" rel="bookmark"
     title="Permalink to Behind the Scenes of this Blog">
    Behind the Scenes of this Blog
  </a>
</h1><div class="article-header-info">
  <p>
      Posted by <a href="/author/fedops.html">fedops</a>
    in 
    <a href="/category/howto.html">
      Howto</a>
    &nbsp;&nbsp;
  </p>
</div> <!-- /.article-header-info -->  </header>
  <div class="content-body" itemprop="text articleBody">
	<p>With the <a href="/moving-to-codeberg.html">move of this blog to
Codeberg</a> I also changed the
process of producing content. In fact one of the drivers was to redo this exact
part. In this post I'd like to give an overview of the setup of the blog and the
process of generating content for it.</p>
<h1 id="data">Data</h1>
<p>The basis for everything, as is so often the case these days, are text files in
one of the low-barrier-of-entry markup languages. Two favorites are obviously
Markdown and reStructured Text, though a large number of great alternatives
exist. To name a few; SGML, Docbook, and a number of Wiki language dialects.</p>
<div class="toc"><span class="toctitle">Table of Contents</span><ul>
<li><a href="#data">Data</a></li>
<li><a href="#the-server">The Server</a></li>
<li><a href="#the-generator">The Generator</a></li>
<li><a href="#the-workflow">The Workflow</a><ul>
<li><a href="#setup">Setup</a></li>
<li><a href="#create-content">Create Content</a></li>
<li><a href="#local-processing-and-proofreading">Local Processing and Proofreading</a></li>
<li><a href="#publishing">Publishing</a></li>
</ul>
</li>
<li><a href="#wrapup">Wrapup</a></li>
</ul>
</div>
<p>The reasons for using them are always the same and you've heard them a number of
times: no special tools needed, easy to create and consume, straightforward
version management. And the availability of processing tools.</p>
<p>For now I have chosen to (continue to) use Markdown, though maybe reST or others
become an option in the future.</p>
<h1 id="the-server">The Server</h1>
<p>I believe in the simplicity and security of minimal web servers that only serve
static content. Over the past decades web servers have mushroomed to become
monster creations. Server-side processors generating highly dynamic content,
backed by database backends and incorporating myriad frameworks with
dependency lists that are longer than a restaurant menu. Not only are they a
burden to build and operate, they also consume a lot of unnecessary bandwidth
with single "pages" that are easily in the 5-10 Megabyte range. And obviously
all that processing power and transmission capacity needs to be powered somehow,
wasting lots of energy and other scarce resources.</p>
<p>These creations tend to exist as commercial websites that leverage the power of
the frameworks to serve ads, trackers, and other kinds of nefarious code to the
visitors' browsers. Many owners of niche websites and blogs have teamed up in
groups such as the <a href="https://512kb.club/">512kb Club</a> to counteract the bloat.
Content should be front and center. I personally surf with Javascript turned off
for security reasons most of the time so I expect sites to be readable that way.
And obviously, so should be my own.</p>
<p>Static sites are easy to serve, and a number of Git forges provide such a
service. For example Codeberg, a hosted Gitea-descendant operated by a German
non-profit. They <a href="https://join.codeberg.org/">are accepting contributing
membership</a>, BTW.</p>
<h1 id="the-generator">The Generator</h1>
<p>To go from data to web site, you need a Static Site Generator (SSG). There are
literally <a href="https://jamstack.org/generators/">hundreds to chose from</a> and new
ones come out what seems like every week. They range from bare bones to most
ornate, written in every language under the sun, and come in all flavors
including prototype and abandonware. Luckily a number are really mature and
full-featured and have been used in production for years.</p>
<p>After looking at heavyweights such as Hugo and Eleventy I became aware of
Pelican. Written in Python and with well over a decade under its belt there's a
rich assortment or plugins and themes available as well. Installation is covered
well <a href="https://docs.getpelican.com/en/latest/quickstart.html">here</a> so I won't
repeat it. In keeping with the idea of a minimal setup I only installed a
single plugin, <code>pelican-neighbors</code>, via <code>pip3</code>.</p>
<h1 id="the-workflow">The Workflow</h1>
<p>These are the actual steps for the blog creation and maintenance.</p>
<h2 id="setup">Setup</h2>
<p>To produce and publish content, the first step is to run <code>pelican-quickstart</code> to
set up the basic project structure. In my case the root is in
<code>~/src/local/blog</code>. All content goes into the <code>content/</code> subdirectory - create or
copy your blog posts (called "articles") directly there. Static content
such as the "about" page or anything else that isn't chronological goes into
<code>content/pages/</code>.</p>
<p>Lastly, if you want to theme your site, select one or more themes e.g. from
<a href="https://github.com/getpelican/pelican-themes">the Pelican Themes github</a> or
create your own and move them into <code>theme-name/</code>. Basically, your directory
hierarchy ends up looking like this (<code>voidy-bootstrap</code> is the name of the theme
I'm using):</p>
<pre><code class="language-bash">~/src/local/blog
├── content
│   ├── ...articles go here...
│   ├── images
│   │   ├── ...images go here...
│   └── pages
│       └── ...static pages go here...
├── Makefile
├── output --&gt; symlink
│   └── ...generated HTML pages...
├── pelicanconf.py
├── publishconf.py
├── tasks.py
└── voidy-bootstrap
    └── ...all the theme files...
</code></pre>
<p>A couple of tips:</p>
<ol>
<li>if you obtain the theme from a git repo somewhere, I'd suggest to break the
   link to that repo and just use the files. You'll probably want to customize
   it eventually and make it your own, so I find it easier to keep it under my
   control.</li>
<li>if you want to <code>git push</code> your site then create the <code>output/</code> directory
   outside of this tree and create a symbolic link to it. That way the generated
   content doesn't plug up your site source repo.</li>
<li>initialize the blog folder as a git repo after setting it up, including the
   theme(s) subdirectory.</li>
<li>initialize the <code>output/</code> folder as a separate repo. This one is being used to
   push the generated content to Codeberg.</li>
</ol>
<h2 id="create-content">Create Content</h2>
<p>Create Markdown or reStructured Text source files as usual. Pelican makes use of
headers such as Title, Date, and Status to steer processing operations - check
the documentation.</p>
<h2 id="local-processing-and-proofreading">Local Processing and Proofreading</h2>
<p>Once you are done with the draft of your new page it's time to process it. If
you elected to let the quickstart process generate a Makefile, then you can just
run <code>make devserver</code> in a separate terminal session. A local webserver will be
started on 127.0.0.1 port 8000 and the output directory will be populated with
the processed HTML pages. Point your web browser there to see the rendered
output. Note that at this point everything is still local and your actual web
site has not been altered.</p>
<p>Leave the devserver running while you make changes to your page(s). Upon saving
it will re-process the files and a simple reload in your web browser will show
the updates. Very convenient! Here's a view of the process in action, editing
this very page:</p>
<p><img alt="editing and displaying a blog page" src="/images/editing-devserver.png" style="max-width: 100%"></p>
<p>Don't forget to also commit your changes to the site source repo.</p>
<h2 id="publishing">Publishing</h2>
<p>Once you are satisfied you want to push the update to your site. First run a
<code>make publish</code> to do final site generation. Then, depending on your preferences
you can use any transfer method your site offers. A great way is to use <code>rsync</code>
over ssh.</p>
<p>Another option, and the default if you host on Codeberg, is to treat <code>output/</code>
as a local repo for your Codeberg Pages repo (hence tip 2 above). In this case
all you need to do is a <code>git add -A; git commit -m "message"; git push</code>.</p>
<blockquote>
<p>If you do go the git way, the <code>publishconf.py</code> configuration file should contain
the line <code>OUTPUT_RETENTION = [".git"]</code> to preserve the <code>.git</code> subdirectory from
being erased as part of the publishing process.</p>
</blockquote>
<h1 id="wrapup">Wrapup</h1>
<p>I have modified the <code>voidy-bootstrap</code> theme somewhat, mostly removing everything
to do with sharing on Facebook and Twitter and tweaking the font settings a
bit. While the included CSS files are only around 4kB, it is still loading too
much stuff from Bootstrap itself for my liking. As it stands every single page
is around 20kB of HTML source, 4kB of site-local CSS, and around 160kB of
Bootstrap and Fontawesome CSS combined. Still a ticket to the 512kB club but can
clearly be further reduced as a next step.</p>
<p>As for plugins the
<a href="https://github.com/pelican-plugins/neighbors">pelican-neighbors</a> is really
useful as it provides previous/next article link objects which I have also
patched into voidy. You can see these at the bottom of each article. This
statement in <code>pelicanconf.py</code> activates the include: <code>CUSTOM_ARTICLE_FOOTERS =
("taglist.html", "previousnext.html")</code> while the code in
<code>voidy-bootstrap/templates/includes/previousnext.html</code> looks like this:</p>
<pre><code class="language-html">&lt;!-- via neighbor plugin, see: https://github.com/pelican-plugins/neighbors --&gt;
    &lt;hr /&gt;
    &lt;p class=&quot;content-emphasis&quot;&gt;
    &lt;table width=&quot;100%&quot;&gt;
       &lt;tr&gt;
            &lt;td align=&quot;left&quot;&gt;
    {% if article.prev_article %}
            &lt;a href=&quot;{{ SITEURL }}/{{ article.prev_article.url}}&quot;&gt;
                &lt;i class=&quot;fas fa-arrow-circle-left fa-fw fa-lg&quot;&gt;&lt;/i&gt;
                    {{ article.prev_article.title }}
            &lt;/a&gt;
    {% endif %}
           &lt;/td&gt;
           &lt;td align=&quot;right&quot;&gt;
    {% if article.next_article %}
           &lt;a href=&quot;{{ SITEURL }}/{{ article.next_article.url}}&quot;&gt;
                {{ article.next_article.title }}
                    &lt;i class=&quot;fas fa-arrow-circle-right fa-fw fa-lg&quot;&gt;&lt;/i&gt;
            &lt;/a&gt;
    {% endif %}
           &lt;/td&gt;
        &lt;/tr&gt;
    &lt;/table&gt;
    &lt;/p&gt;
</code></pre>
<p>Having worked with this for a short while now I'm happy. The initial setup and
familiarization took a few hours of reading and experimenting, but that's the
fun part of course. Pelican works as expected and is quite configurable. Being
Python, it would be easy enough to modify further should that be required at
a future time. Processing speed is really good, the whole 39-article
site builds in a matter of milliseconds, so totally not an issue even on a
Raspberry Pi. I'm fully convinced you could even run this in Termux on an
Android phone if you really wanted.</p>
<p>Mission accomplished I'd say.</p>
  </div>
  
<div class="article-tag-list">
<span class="label label-default">Tags</span>
	<a href="/tag/blog.html"><i class="fas fa-tag"></i>blog</a>&nbsp;
	<a href="/tag/software.html"><i class="fas fa-tag"></i>software</a>&nbsp;
	<a href="/tag/web.html"><i class="fas fa-tag"></i>web</a>&nbsp;
	<a href="/tag/housekeeping.html"><i class="fas fa-tag"></i>housekeeping</a>&nbsp;
</div><!-- via neighbor plugin, see: https://github.com/pelican-plugins/neighbors -->
    <hr />
    <p class="content-emphasis">
	<table width="100%">
	    <tr>
			<td align="left">
	        </td>
	        <td align="right">
	        </td>
		</tr>
	</table>
	</p>
</article>
        </div><!-- /content -->

        <div class="col-md-3 sidebar-nav" id="sidebar">

<div class="row">

<div class="col-xs-6 col-md-12">
<h4><i class="fas fa-comment fa-fw fa-lg"></i> Social</h4>
<ul class="list-unstyled social-links">
    <li><a href="https://fosstodon.org/@fedops" target="_blank">
	  <i class="fas fa-comments fa-fw fa-lg" title="Fosstodon"></i>
		Fosstodon
	</a></li>
    <li><a href="https://codeberg.org/fedops" target="_blank">
	  <i class="fas fa-comments fa-fw fa-lg" title="Codeberg"></i>
		Codeberg
	</a></li>
</ul>
</div>

<div class="col-xs-6 col-md-12">
<h4><i class="fas fa-folder fa-fw fa-lg"></i> Categories</h4>
<ul class="list-unstyled category-links">
  <li><a href="/category/cloud.html" >
    <i class="fas fa-folder-open fa-fw fa-lg"></i> Cloud</a></li>
  <li><a href="/category/hardware.html" >
    <i class="fas fa-folder-open fa-fw fa-lg"></i> Hardware</a></li>
  <li><a href="/category/howto.html" >
    <i class="fas fa-folder-open fa-fw fa-lg"></i> Howto</a></li>
  <li><a href="/category/infomanagement.html" >
    <i class="fas fa-folder-open fa-fw fa-lg"></i> InfoManagement</a></li>
  <li><a href="/category/media.html" >
    <i class="fas fa-folder-open fa-fw fa-lg"></i> Media</a></li>
  <li><a href="/category/misc.html" >
    <i class="fas fa-folder-open fa-fw fa-lg"></i> misc</a></li>
  <li><a href="/category/phone.html" >
    <i class="fas fa-folder-open fa-fw fa-lg"></i> Phone</a></li>
  <li><a href="/category/privacy.html" >
    <i class="fas fa-folder-open fa-fw fa-lg"></i> Privacy</a></li>
  <li><a href="/category/security.html" >
    <i class="fas fa-folder-open fa-fw fa-lg"></i> Security</a></li>
  <li><a href="/category/software.html" >
    <i class="fas fa-folder-open fa-fw fa-lg"></i> Software</a></li>
</ul>
</div>

</div> <!-- /row -->

  <h4><i class="fas fa-link fa-fw fa-lg"></i> Links</h4>
  <ul class="list-unstyled category-links">
    <li><a href="https://getpelican.com/" >
      <i class="fas fa-external-link-square-alt fa-fw fa-lg"></i> Pelican</a></li>
    <li><a href="https://www.python.org/" >
      <i class="fas fa-external-link-square-alt fa-fw fa-lg"></i> Python.org</a></li>
    <li><a href="https://palletsprojects.com/p/jinja/" >
      <i class="fas fa-external-link-square-alt fa-fw fa-lg"></i> Jinja2</a></li>
  </ul>
<h4><i class="fas fa-tags fa-fw fa-lg"></i> Tags</h4>
<p class="tag-cloud">
      <a href="/tag/linux.html">
        <i class="fas fa-tag fa-fw fa-lg"></i>linux
      </a>
      <a href="/tag/os.html">
        <i class="fas fa-tag fa-fw fa-lg"></i>os
      </a>
      <a href="/tag/fedora.html">
        <i class="fas fa-tag fa-fw fa-lg"></i>fedora
      </a>
      <a href="/tag/networking.html">
        <i class="fas fa-tag fa-fw fa-lg"></i>networking
      </a>
      <a href="/tag/wireguard.html">
        <i class="fas fa-tag fa-fw fa-lg"></i>wireguard
      </a>
      <a href="/tag/privacy.html">
        <i class="fas fa-tag fa-fw fa-lg"></i>privacy
      </a>
      <a href="/tag/media.html">
        <i class="fas fa-tag fa-fw fa-lg"></i>media
      </a>
      <a href="/tag/hardware.html">
        <i class="fas fa-tag fa-fw fa-lg"></i>hardware
      </a>
      <a href="/tag/software.html">
        <i class="fas fa-tag fa-fw fa-lg"></i>software
      </a>
      <a href="/tag/smarthome.html">
        <i class="fas fa-tag fa-fw fa-lg"></i>smarthome
      </a>
      <a href="/tag/sustainability.html">
        <i class="fas fa-tag fa-fw fa-lg"></i>sustainability
      </a>
      <a href="/tag/mqtt.html">
        <i class="fas fa-tag fa-fw fa-lg"></i>mqtt
      </a>
      <a href="/tag/photography.html">
        <i class="fas fa-tag fa-fw fa-lg"></i>photography
      </a>
      <a href="/tag/security.html">
        <i class="fas fa-tag fa-fw fa-lg"></i>security
      </a>
      <a href="/tag/mobile.html">
        <i class="fas fa-tag fa-fw fa-lg"></i>mobile
      </a>
      <a href="/tag/quicktip.html">
        <i class="fas fa-tag fa-fw fa-lg"></i>quicktip
      </a>
      <a href="/tag/sun.html">
        <i class="fas fa-tag fa-fw fa-lg"></i>sun
      </a>
      <a href="/tag/arduino.html">
        <i class="fas fa-tag fa-fw fa-lg"></i>arduino
      </a>
      <a href="/tag/web.html">
        <i class="fas fa-tag fa-fw fa-lg"></i>web
      </a>
      <a href="/tag/bookmarks.html">
        <i class="fas fa-tag fa-fw fa-lg"></i>bookmarks
      </a>
      <a href="/tag/documentation.html">
        <i class="fas fa-tag fa-fw fa-lg"></i>documentation
      </a>
      <a href="/tag/blog.html">
        <i class="fas fa-tag fa-fw fa-lg"></i>blog
      </a>
      <a href="/tag/housekeeping.html">
        <i class="fas fa-tag fa-fw fa-lg"></i>housekeeping
      </a>
      <a href="/tag/markdown.html">
        <i class="fas fa-tag fa-fw fa-lg"></i>markdown
      </a>
      <a href="/tag/cloud.html">
        <i class="fas fa-tag fa-fw fa-lg"></i>cloud
      </a>
      <a href="/tag/spyware.html">
        <i class="fas fa-tag fa-fw fa-lg"></i>spyware
      </a>
      <a href="/tag/phone.html">
        <i class="fas fa-tag fa-fw fa-lg"></i>phone
      </a>
      <a href="/tag/applications.html">
        <i class="fas fa-tag fa-fw fa-lg"></i>applications
      </a>
      <a href="/tag/chat.html">
        <i class="fas fa-tag fa-fw fa-lg"></i>chat
      </a>
      <a href="/tag/mastodon.html">
        <i class="fas fa-tag fa-fw fa-lg"></i>mastodon
      </a>
      <a href="/tag/gui.html">
        <i class="fas fa-tag fa-fw fa-lg"></i>gui
      </a>
      <a href="/tag/application.html">
        <i class="fas fa-tag fa-fw fa-lg"></i>application
      </a>
</p>

<hr />

        </div><!--/sidebar -->
      </div><!--/row-->
    </div><!--/.container /#main-container -->

    <footer id="site-footer">
 
      <address id="site-colophon">
        <p class="text-center text-muted">
        Site built using <a href="http://getpelican.com/" target="_blank">Pelican</a>
        &nbsp;&bull;&nbsp; Theme based on
        <a href="http://www.voidynullness.net/page/voidy-bootstrap-pelican-theme/"
           target="_blank">VoidyBootstrap</a> by 
        <a href="http://voidynullness.net"
           target="_blank">RKI</a>  
        </p>
      </address><!-- /colophon  -->
    </footer>


    <!-- javascript -->
   
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js" integrity="sha384-nvAa0+6Qg9clwYCGGPpDQLVpLNn0fRaROjHqs13t4Ggj3Ez50XnGQqc/r8MhnRDZ" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
            integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
            crossorigin="anonymous"></script>


  </body>
</html>