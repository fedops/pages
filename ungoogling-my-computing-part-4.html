<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta name="description" content="" />
    <meta name="author" content="fedops" />
    <meta name="generator" content="Pelican (VoidyBootstrap theme)" />

    <title>Ungoogling My Computing Part 4 - fedops blog</title>

   
        <link rel="stylesheet"
              href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
              integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u"
              crossorigin="anonymous" />

      <link rel="stylesheet"
            href="https://use.fontawesome.com/releases/v5.0.13/css/all.css"
            integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp"
            crossorigin="anonymous"
      />




      <link rel="stylesheet" href="/theme/css/pygment.css" />
      <link rel="stylesheet" href="/theme/css/voidybootstrap.css" />

    <link rel="shortcut icon" href="/favicon.ico" />
  </head>

  <body>
   
    <nav class="navbar navbar-default">
      <div class="container">
	   <div class="navbar-header">
		<button type="button" class="navbar-toggle" 
				data-toggle="collapse" data-target="#main-navbar-collapse">
		  <span class="sr-only">Toggle navigation</span>
		  <span class="icon-bar"></span>
		  <span class="icon-bar"></span>
		  <span class="icon-bar"></span>
		</button>
		<a class="navbar-brand" href="/" rel="home">
          <i class="fas fa-home fa-fw fa-lg"> </i> </a>
       </div>

      <div class="collapse navbar-collapse" id="main-navbar-collapse">
        <ul class="nav navbar-nav">
              <li>
                <a href="/pages/about-this-blog.html">About this Blog</a>
              </li>
            <li class="divider"></li>
            <li class="">
              <a href="/archives.html">Archives</a>
            </li>
          <li class="divider"></li>
        </ul> <!-- /nav -->
      </div> <!-- /navbar-collapse -->
	  </div> <!-- /container -->
    </nav> <!-- /navbar -->

	<div class="jumbotron" id="overview">
	  <div class="container">
		<h1><a href="/">fedops blog</a></h1>
		<p class="lead">Privacy in Computing</p>
	  </div>
	</div>

    <div class="container" id="main-container">
      <div class="row">
        <div class="col-md-9" id="content">
<article itemscope="itemscope" itemtype="http://schema.org/BlogPosting">
  <header class="article-header">
<abbr class="article-header-date">
  Tue 13 July 2021
</abbr> <h1>
  <a href="/ungoogling-my-computing-part-4.html" rel="bookmark"
     title="Permalink to Ungoogling My Computing Part 4">
    Ungoogling My Computing Part 4
  </a>
</h1><div class="article-header-info">
  <p>
      Posted by <a href="/author/fedops.html">fedops</a>
    in 
    <a href="/category/phone.html">
      Phone</a>
    &nbsp;&nbsp;
  </p>
</div> <!-- /.article-header-info -->  </header>
  <div class="content-body" itemprop="text articleBody">
	<p><em>This is part 4 in a series of getting rid of Google in my computing. See the
beginning and index here:
<a href="/ungoogling-my-computing.html">Introduction</a>.</em></p>
<p>After the initial setup and a few tests let's dive a little deeper into actually
making the phone usable.</p>
<h1 id="application-sources">Application Sources</h1>
<p>Having established we don't want Google in our lives by buying an ungoogled
phone, we have to get our apps from somewhere. In general there are the
following categories of applications sources:</p>
<h2 id="free-and-open-source-apps-from-f-droid">Free and Open Source Apps from F-Droid</h2>
<p>This is clearly the preferred source of privacy-respecting apps. F-Droid in
particular ensures:</p>
<ul>
<li>license-encumbered "anti features" (i.e., restrictions) are clearly called out
  in the app description before download or installation</li>
<li>reproducible builds ensure clean app packages</li>
<li>all apps are built from source on F-Droid's pipeline so there is no
  possibility for anyone to slip-stream unwanted modifications into the
  package </li>
<li>authors that submit apps to F-Droid are interested in providing a service to
  the community since there is no commercial profit involved</li>
</ul>
<p>There are also some downsides. An obvious one is that there is no payment
model - including no in-app purchases - which makes it difficult to obtain paid versions
of software. For example OSMand is available, but there is no way to upgrade to
OSMand+ since payments can only be made through the Play Store, and only from
the OSMand APK from the Play Store. Commercial software authors thus need to
find and establish alternative ways of paying through software which many are
not willing to do. </p>
<p>From a user's perspective, going with free software requires changes in habits
and ways of working as many of the free apps will work very differently from
their commercial counterparts.</p>
<p>It also has to be said that navigating the F-Droid store takes a bit of getting
used to. There are far fewer helpful steps such as app recommendations.
Sometimes the description of the app is extremely sparse, making it difficult to
understand what it does, and also making it hard to find it in the first place.
Some apps are clearly abandonware, having been in the store for 8 years or more
without an update.<sup id="fnref:1"><a class="footnote-ref" href="#fn:1">1</a></sup> And - my personal pet peeve - apps without screenshots so
you cannot see what it looks like before installing it.</p>
<h2 id="developer-packaged-downloads">Developer-Packaged Downloads</h2>
<p>A number of projects such as Firefox Focus, Session, Signal, etc. offer download
locations linked to their source code repos. For example, on their Github sites
or their own web servers. This offers the "purest" downloads without any
unwanted additives, but it generally requires manual updates unless a kind soul
provides a meta-installer such as FFupdate. This really is your second-best bet
after F-Droid.</p>
<h2 id="alternative-app-stores">"Alternative" App Stores</h2>
<p>There is a plethora of alternative app stores which offer popular apps for
download that aren't otherwise available except through the Play Store. Among
them are ApkPure, Aptoide, APKMonk, Apktada, and others.</p>
<p>These stores all obtain the actual APK files (presumably) from the Play Store
and generally repackage them. Their business model operates on a combination of
advertisements and data stealing which is forwarded to a host of tracking sites
and other privacy offenders such as Facebook. All of them offer their own Store
app which looks and works like the Play Store and makes it easy to find,
install, and update apps from their respective stores.</p>
<p>Contrary to Google's Play Store they do not require user registration and as
such are somewhat more privacy-respecting. However, these stores are granted
highest privileges due to their nature of needing them to install apps into the 
system. This opens the door to all kinds of nefarious activities.</p>
<p>My recommendation is to not use any of these store apps, but download the APKs
themselves and sideload them, see below.</p>
<p>A special case of these stores is the Amazon App Store. Amazon regularly
discounts apps that are installed through this store. For example years ago a
navigation app that retailed for $50 was available for free "for a limited
time". As always with discounts, this is a trap - in this case to lure people into
installing the App Store. Armed with system privileges the store transmitted
both the Amazon ID as well as the Facebook ID home, with the result that on the
next day I started seeing Amazon ads for products I might be interested in in my
Facebook feed. Not cool, and one reason I use neither Facebook nor Amazon
anymore.</p>
<h2 id="side-loading">Side Loading</h2>
<p>APKs can be side loaded -- installed without any store app simply from an
application so permitted. This is straightforward for APKs.</p>
<p>For apps distributed as XAPKs the situation is a bit more complicated. XAPKs are
actually ZIP files with a different suffix. Viewing the contents e.g. via <code>unzip
-l file.xapk</code> shows the contents, which will usually be one or more .apks, a
manifest file, and possibly a Cache directory with media files. The format was
originally targetted towards installations of large games with many megabytes of
graphics files. However, many of the alternative App Stores use this format
because it will lead people to use their Store Apps to install them as the
normal Android system utilities don't support them.</p>
<p>Standalone XAPK installers are available from different sources, all of which
are not especially confidence-inspiring. The Aptoide XAPK installer is
especially noteworthy as in fact it is a modified version of their Store App
which, after installation, prompts to download and install the actuall XAPK
installer itself. Sneaky! Since the "damage" had been done I elected to keep the
XAPK installer and then delete the Aptoide Store App again, leaving me with just
the installer for side loading XAPKs.<sup id="fnref:2"><a class="footnote-ref" href="#fn:2">2</a></sup></p>
<p>One thing to keep in mind with side loading is that there is no easy way to keep
applications updated, or even being notified of available updates. While this
keeps unwanted updates away it does the same for wanted security fixes.</p>
<h2 id="trackers">Trackers</h2>
<p>The App Store apps are of course riddled with trackers. Aptoide's wasn't too
bad, it tried to contact these servers in addition to Aptoide's own:</p>
<pre><code>ads.mopub.com
cdn2.inner-active.mobi
config.inmobi.com
diagnostics.rakam.io
graph.facebook.com
webservices.aptwords.net
</code></pre>
<p>The XAPK installer itself is relatively benign. Other than the usual
graph.facebook.com the only other contacted endpoints were in the
apkrep.avcdn.net domain which are CNAMEs for file replica servers of
avast.com, an antivirus software company.</p>
<h2 id="app-installation-methodology">App Installation Methodology</h2>
<p>With these things considered I suggest the following approach to app
installation:</p>
<ol>
<li>Search for something you want or need in the F-Droid store. If you can get it
   there, install it from there and all is well. If you don't find <em>exactly</em>
   what you're looking for but something close to it, consider if it's maybe
   good enough and worth adjusting your ways of working for and to.</li>
<li>Failing that, find a suitable download location for the app in question from
   the developer site.</li>
<li>For many online services, it may in fact be good enough to use the web
   interface with a browser app which you already have installed instead of
   loading yet another app that brings with it more problems than it solves.</li>
<li>If that is not an option and the app is only available via Google Play or any of
   the "alternative" apps stores, find the download location of least mistrust.
   I suggest to not use any of the app store installers, and not to perform the
   installation directly on the phone.</li>
</ol>
<p>For what I call "offboard sideloading" I use my Linux PC to do the research and
then download the APK package, sometimes in multiple versions from different
sites. Then I will unzip the APKs to inspect the packages and make sure I
toss out the ones containing additional stuff, such as other (sub-)packages.</p>
<p>What I think is a good package will then get copied to a special directory on my
PC where I keep the sideloaded packages. I also keep a text file which lists the
package names and where I got each one from so I can check for updated versions
every now and then.</p>
<p>The whole directory gets rsync-ed to the phone (more on that later), and then I
will manually install the package using a file manager which has been granted the
permission to do so.</p>
<h1 id="applications">Applications</h1>
<p>Here are some notes on applications I use and recommend. Obviously your mileage
may vary massively.</p>
<h2 id="installed-apps-from-f-droid">Installed apps from F-Droid:</h2>
<ul>
<li>F-Droid store and Foxydroid</li>
<li>Wireguard</li>
<li>Florisboard</li>
<li>Firefox Focus</li>
<li>Privacy Browser</li>
<li>Fennec (Firefox)</li>
<li>SimpleSSHd</li>
<li>KeepassDX</li>
<li>andOTP</li>
<li>Exodus</li>
<li>SatStat for GPS status/simple OSM map interface</li>
<li>Trigger (Hot Button replacement)</li>
<li>FOSS weather apps: AF Weather Widget, Tiny Weather Forecast</li>
<li>Port Authority as a DNS/Ping/... checker replacement for Net Tools (doesn't run without GMS)</li>
<li>Maps &amp; GPS Navigation OSMand+</li>
</ul>
<p>All of these are really useful and full replacements for equivalents in the Play
Store, or in some cases are the exact same apps as in the Play Store but
reproducibly built by F-Droid.</p>
<h2 id="installed-apps-directly-from-developers-sources">Installed apps directly from developers' sources:</h2>
<ul>
<li>Signal (with background checks to replace Google notifications)</li>
<li>Session (APK from getsession.org, Google notifications disabled)</li>
</ul>
<p>These are the two messengers I use all the time, tracker-free direct from the
developers.</p>
<h2 id="installed-apps-from-apkpure">Installed apps from APKPure:</h2>
<ul>
<li>Podcast Addict (runs without GMS)                             </li>
<li>Visimarks bookmark manager (see below)                        </li>
</ul>
<p>These are the "apps of shame". I really like Podcast Addict for all its useful
features and have a hard time adjusting to any of the free alternatives such as
Antennapod. They are really good also, I have just very much gotten used to PA.
The APKPure version works without Google Mobile Services but comes drenched in
trackers... Visimarks is a nice little browser-independent bookmark manager that
works completely offline. Unfortunately also chock-full of trackers.</p>
<p>The standard app installer tries to perform a malware scan during each
installation. It operates on increasingly old signatures since access to the AV
scan sites is blocked: <em>caveat emptor</em>.</p>
<h2 id="installed-apps-from-apktada">Installed apps from Apktada:</h2>
<ul>
<li><a href="https://www.ulfdittmer.com/view?PingNetFaq">Ping &amp; Net</a> (the best all-around network testing/debugging tool)</li>
</ul>
<p>Ping &amp; Net is extremely useful. Unfortunately the author does not offer a direct
download.</p>
<h2 id="notes-on-browsers">Notes on Browsers</h2>
<p>Everybody's browsing habits are different. I feel the subject is important
enough to ponder it for a bit.</p>
<p>Browsers today are more application platforms than simple web page display
tools. Many web sites require to store data such as cookies and run code,
usually Javascript, in the browser to function properly. If you read this blog
you're probably interested in protecting your privacy, and the most likely
reason you have become interested is because of web sites snooping on your
data.</p>
<p>To be very clear: browsing the web with things like Javascript enabled you are
basically allowing anyone out in the wild Interwebs to run their code on your
device. And you are putting the trust to protect you from any dire consequences
into both the web site operators' hands as well as the hands of the browser
developer. I believe neither trust is warranted.</p>
<p>For the website operators, you are the product they're selling. Web hits
translate into ad revenue, and proper targeting of such ads increases the
revenue. So they will do as much as they can to create every-improving
profiles of their visitors in order to target ads more effectively. Such
targeted ad space is then sold to ad network operators, in some cases by
real-time auctioning it off. Those operators will pull every dirty trick in
their book to turn this expensive space into money -- by way of tracking you
across sites, learning your habits and preferences, and thus adding to your
profiles with them.</p>
<p>For the browser developers it is a constant rat race to keep up with what
everybody else comes up with all the time to defeat your defences and enhance
their profit.</p>
<p>There are lots of countermeasures. The first ones we're concerned with on our
phones are:</p>
<ol>
<li>disable any features you don't need for <em>your</em> purposes. This includes
   turning off Javascript, cookies, and as much of session persistance as
   possible.</li>
<li>compartmentalize your browsing. On the PC, Firefox with Multi-Account
   Containers and tab isolation works really well, but on phones with their
   somewhat limited resources the best you can do is use multiple separate
   browsers, each for their own purpose and configured accordingly.</li>
<li>limit your browsing to exclude access to confidential data. Don't do online
   banking, don't log into sites with critical data, don't reuse credentials you
   have reserved for higher security classes.</li>
</ol>
<p>The practical upshot of this: install and consciously use several different
browsers. Here's what I have and use:</p>
<p><strong>Privacy Browser:</strong> has everything disabled by default which will break tons of
websites but offers the best protection. This is the default browser in the
phone that gets opened whenever a URL is selected from any other app. Every
disabled feature can be turned on individually and these settings can even be
stored in site profiles. It works fine for reading news sites and actually
around 80% of the sites I visit.</p>
<p><strong>Firefox Focus:</strong> much less intrusive, works well in standard mode with sites
like Ebay and other sites that require some Javascript to function. It "forgets"
all stored session data immediately and thus always comes up completely clean
and "brainwashed".</p>
<p><strong>Bromium:</strong> being a Chrome decendant it fulfills 95% of use cases for which a
Chrome browser is required, without the Google baggage.</p>
<p><strong>Fennec:</strong> a Firefox decendant this is the most full-featured
browser and the one I use for things that require maintaining state between
sessions. For example I have a Flightradar24 account and their app doesn't work
without GMS. But in Fennec, their web site is perfectly useable. I have stored
my login information in the browser as it's not at all critical and quite
convenient. Fennec as well as other browsers allow you to place an app icon on
the phone's launcher which when tapped will load the web site directly into the
browser. That's the closest you can get to an app without having an app. ;-)</p>
<p><strong>FOSS Browser:</strong> an Android WebView-engined browser based on Ninja. I use this
for browsing resources in my local network only. For that reason it's set as an
excluded app in the Wireguard tunnel config.</p>
<p>All browsers have their usual 3rd party cookie blocking and other defences
turned on. It's important to start the correct browser for the use case you need
it for. I generally start with Privacy Browser and then work my way up the chain
when something doesn't quite work. Keep in mind I don't do any online shopping
or home banking so again, YMMV.</p>
<p>The FFupdater app on F-Droid is a nice companion to help install and keep
multiple versions of Firefox-decendants updated, among them Firefox Focus,
Iceraven, and others.  Despite its name it will also install and update the
Chromium-variant Bromite.  This saves the hassle of having to manually scan
their various Github repos.</p>
<p>The included Huawei browser BTW is an ungoogled Chromium, much like Bromium -
useful to have in certain cases. The Huawei version, like many other apps, tries
to connect to a plethora of Huawei cloud servers right at startup -- since those
are blocked it will quite often indicate "offline", but a refresh solves that. I
sometimes use it but generally Bromium is the better option.</p>
<p>A final word on browsers: all the listed ones except Bromite support the
function "Add to Home Screen", sometimes in the hamburger menu or the sharing
menu. This will create an icon on the home screen that when tapped starts the
selected browser with the site URL loaded. With a well-behaved website this is
fairly close to an actual app installation. This example shows one icon each for
a local web page in Foss Browser, FlightRadar24 in Fennec, and Google Maps<sup id="fnref:3"><a class="footnote-ref" href="#fn:3">3</a></sup> in
the Huawei Chrome browser:</p>
<p><img alt="Browser icons" src="/images/browser-icons.jpg" style="max-width: 100%"></p>
<h1 id="findings-considerations-so-far">Findings &amp; Considerations So Far</h1>
<p>Installing apps from stores such as F-Droid and ApkPure or side loading APK
downloads is relatively straightforward. Some apps refuse to work after installation
because there are no Google Mobile Services present. Installation of MicroG can
be used to get around this limitation, but that is not desirable.</p>
<p>All app downloads must be made from sources that require no user account to
prevent microtargetting with specially crafted downloads. The risk of otherwise
boobytrapped apps including e.g. trojans must be carefully weighed.</p>
<p>While F-Droid apps are vetted, apps from ApkPure and other stores include a
number of trackers which try to contact questionable sites. An app checker such
as Exodus should be used to investigate, and proper DNS blocks must be in place and
kept updated to prevent such traffic. This is a moving target.</p>
<p>After app installation a test should be made to see what outgoing connections
are initiated by it and which of these should be blocked. Newer versions of
Android offer the possibility to control network access rights for cellular or
Wifi, and for specific SSIDs. Apps that do not need to access the network should
have these permissions revoked by default.</p>
<h1 id="conclusion">Conclusion</h1>
<p>This was a rather application-centric treatise. Join us again next time when we
talk about backups, do some more network sniffing and get into location services
and VoWifi.</p>
<div class="footnote">
<hr>
<ol>
<li id="fn:1">
<p>which does not have to be a bad thing - maybe there's just nothing to fix or
improve - but that's at least unusual.&#160;<a class="footnote-backref" href="#fnref:1" title="Jump back to footnote 1 in the text">&#8617;</a></p>
</li>
<li id="fn:2">
<p>the same is true for any app downloaded from the Aptoide and Apkpure
stores via the web page. Be very careful what you download before shoving it
onto the phone, and never download from the phone directly.&#160;<a class="footnote-backref" href="#fnref:2" title="Jump back to footnote 2 in the text">&#8617;</a></p>
</li>
<li id="fn:3">
<p>yes, I occasionally do use Google for one thing: Maps. The
live traffic feature is just too convenient when driving. Access to the Maps
server endpoints is allowed in my firewall ruleset.&#160;<a class="footnote-backref" href="#fnref:3" title="Jump back to footnote 3 in the text">&#8617;</a></p>
</li>
</ol>
</div>
  </div>
  
<div class="article-tag-list">
<span class="label label-default">Tags</span>
	<a href="/tag/privacy.html"><i class="fas fa-tag"></i>privacy</a>&nbsp;
	<a href="/tag/hardware.html"><i class="fas fa-tag"></i>hardware</a>&nbsp;
	<a href="/tag/software.html"><i class="fas fa-tag"></i>software</a>&nbsp;
	<a href="/tag/phone.html"><i class="fas fa-tag"></i>phone</a>&nbsp;
	<a href="/tag/wireguard.html"><i class="fas fa-tag"></i>wireguard</a>&nbsp;
</div><!-- via neighbor plugin, see: https://github.com/pelican-plugins/neighbors -->
    <hr />
    <p class="content-emphasis">
	<table width="100%">
	    <tr>
			<td align="left">
	        </td>
	        <td align="right">
	        </td>
		</tr>
	</table>
	</p>
</article>
        </div><!-- /content -->

        <div class="col-md-3 sidebar-nav" id="sidebar">

<div class="row">

<div class="col-xs-6 col-md-12">
<h4><i class="fas fa-comment fa-fw fa-lg"></i> Social</h4>
<ul class="list-unstyled social-links">
    <li><a href="https://fosstodon.org/@fedops" target="_blank">
	  <i class="fas fa-comments fa-fw fa-lg" title="Fosstodon"></i>
		Fosstodon
	</a></li>
    <li><a href="https://codeberg.org/fedops" target="_blank">
	  <i class="fas fa-comments fa-fw fa-lg" title="Codeberg"></i>
		Codeberg
	</a></li>
</ul>
</div>

<div class="col-xs-6 col-md-12">
<h4><i class="fas fa-folder fa-fw fa-lg"></i> Categories</h4>
<ul class="list-unstyled category-links">
  <li><a href="/category/cloud.html" >
    <i class="fas fa-folder-open fa-fw fa-lg"></i> Cloud</a></li>
  <li><a href="/category/hardware.html" >
    <i class="fas fa-folder-open fa-fw fa-lg"></i> Hardware</a></li>
  <li><a href="/category/howto.html" >
    <i class="fas fa-folder-open fa-fw fa-lg"></i> Howto</a></li>
  <li><a href="/category/infomanagement.html" >
    <i class="fas fa-folder-open fa-fw fa-lg"></i> InfoManagement</a></li>
  <li><a href="/category/media.html" >
    <i class="fas fa-folder-open fa-fw fa-lg"></i> Media</a></li>
  <li><a href="/category/misc.html" >
    <i class="fas fa-folder-open fa-fw fa-lg"></i> misc</a></li>
  <li><a href="/category/phone.html" >
    <i class="fas fa-folder-open fa-fw fa-lg"></i> Phone</a></li>
  <li><a href="/category/privacy.html" >
    <i class="fas fa-folder-open fa-fw fa-lg"></i> Privacy</a></li>
  <li><a href="/category/security.html" >
    <i class="fas fa-folder-open fa-fw fa-lg"></i> Security</a></li>
  <li><a href="/category/software.html" >
    <i class="fas fa-folder-open fa-fw fa-lg"></i> Software</a></li>
</ul>
</div>

</div> <!-- /row -->

  <h4><i class="fas fa-link fa-fw fa-lg"></i> Links</h4>
  <ul class="list-unstyled category-links">
    <li><a href="https://getpelican.com/" >
      <i class="fas fa-external-link-square-alt fa-fw fa-lg"></i> Pelican</a></li>
    <li><a href="https://www.python.org/" >
      <i class="fas fa-external-link-square-alt fa-fw fa-lg"></i> Python.org</a></li>
    <li><a href="https://palletsprojects.com/p/jinja/" >
      <i class="fas fa-external-link-square-alt fa-fw fa-lg"></i> Jinja2</a></li>
  </ul>
<h4><i class="fas fa-tags fa-fw fa-lg"></i> Tags</h4>
<p class="tag-cloud">
      <a href="/tag/linux.html">
        <i class="fas fa-tag fa-fw fa-lg"></i>linux
      </a>
      <a href="/tag/os.html">
        <i class="fas fa-tag fa-fw fa-lg"></i>os
      </a>
      <a href="/tag/fedora.html">
        <i class="fas fa-tag fa-fw fa-lg"></i>fedora
      </a>
      <a href="/tag/networking.html">
        <i class="fas fa-tag fa-fw fa-lg"></i>networking
      </a>
      <a href="/tag/wireguard.html">
        <i class="fas fa-tag fa-fw fa-lg"></i>wireguard
      </a>
      <a href="/tag/privacy.html">
        <i class="fas fa-tag fa-fw fa-lg"></i>privacy
      </a>
      <a href="/tag/media.html">
        <i class="fas fa-tag fa-fw fa-lg"></i>media
      </a>
      <a href="/tag/hardware.html">
        <i class="fas fa-tag fa-fw fa-lg"></i>hardware
      </a>
      <a href="/tag/software.html">
        <i class="fas fa-tag fa-fw fa-lg"></i>software
      </a>
      <a href="/tag/smarthome.html">
        <i class="fas fa-tag fa-fw fa-lg"></i>smarthome
      </a>
      <a href="/tag/sustainability.html">
        <i class="fas fa-tag fa-fw fa-lg"></i>sustainability
      </a>
      <a href="/tag/mqtt.html">
        <i class="fas fa-tag fa-fw fa-lg"></i>mqtt
      </a>
      <a href="/tag/photography.html">
        <i class="fas fa-tag fa-fw fa-lg"></i>photography
      </a>
      <a href="/tag/security.html">
        <i class="fas fa-tag fa-fw fa-lg"></i>security
      </a>
      <a href="/tag/mobile.html">
        <i class="fas fa-tag fa-fw fa-lg"></i>mobile
      </a>
      <a href="/tag/quicktip.html">
        <i class="fas fa-tag fa-fw fa-lg"></i>quicktip
      </a>
      <a href="/tag/sun.html">
        <i class="fas fa-tag fa-fw fa-lg"></i>sun
      </a>
      <a href="/tag/arduino.html">
        <i class="fas fa-tag fa-fw fa-lg"></i>arduino
      </a>
      <a href="/tag/web.html">
        <i class="fas fa-tag fa-fw fa-lg"></i>web
      </a>
      <a href="/tag/bookmarks.html">
        <i class="fas fa-tag fa-fw fa-lg"></i>bookmarks
      </a>
      <a href="/tag/documentation.html">
        <i class="fas fa-tag fa-fw fa-lg"></i>documentation
      </a>
      <a href="/tag/blog.html">
        <i class="fas fa-tag fa-fw fa-lg"></i>blog
      </a>
      <a href="/tag/housekeeping.html">
        <i class="fas fa-tag fa-fw fa-lg"></i>housekeeping
      </a>
      <a href="/tag/markdown.html">
        <i class="fas fa-tag fa-fw fa-lg"></i>markdown
      </a>
      <a href="/tag/cloud.html">
        <i class="fas fa-tag fa-fw fa-lg"></i>cloud
      </a>
      <a href="/tag/spyware.html">
        <i class="fas fa-tag fa-fw fa-lg"></i>spyware
      </a>
      <a href="/tag/phone.html">
        <i class="fas fa-tag fa-fw fa-lg"></i>phone
      </a>
      <a href="/tag/applications.html">
        <i class="fas fa-tag fa-fw fa-lg"></i>applications
      </a>
      <a href="/tag/chat.html">
        <i class="fas fa-tag fa-fw fa-lg"></i>chat
      </a>
      <a href="/tag/mastodon.html">
        <i class="fas fa-tag fa-fw fa-lg"></i>mastodon
      </a>
      <a href="/tag/gui.html">
        <i class="fas fa-tag fa-fw fa-lg"></i>gui
      </a>
      <a href="/tag/application.html">
        <i class="fas fa-tag fa-fw fa-lg"></i>application
      </a>
</p>

<hr />

        </div><!--/sidebar -->
      </div><!--/row-->
    </div><!--/.container /#main-container -->

    <footer id="site-footer">
 
      <address id="site-colophon">
        <p class="text-center text-muted">
        Site built using <a href="http://getpelican.com/" target="_blank">Pelican</a>
        &nbsp;&bull;&nbsp; Theme based on
        <a href="http://www.voidynullness.net/page/voidy-bootstrap-pelican-theme/"
           target="_blank">VoidyBootstrap</a> by 
        <a href="http://voidynullness.net"
           target="_blank">RKI</a>  
        </p>
      </address><!-- /colophon  -->
    </footer>


    <!-- javascript -->
   
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js" integrity="sha384-nvAa0+6Qg9clwYCGGPpDQLVpLNn0fRaROjHqs13t4Ggj3Ez50XnGQqc/r8MhnRDZ" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
            integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
            crossorigin="anonymous"></script>


  </body>
</html>