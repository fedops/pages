<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta name="description" content="" />
    <meta name="author" content="fedops" />
    <meta name="generator" content="Pelican (VoidyBootstrap theme)" />

    <title>My FOSS Photo Workflow - fedops blog</title>

   
        <link rel="stylesheet"
              href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
              integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u"
              crossorigin="anonymous" />

      <link rel="stylesheet"
            href="https://use.fontawesome.com/releases/v5.0.13/css/all.css"
            integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp"
            crossorigin="anonymous"
      />




      <link rel="stylesheet" href="/theme/css/pygment.css" />
      <link rel="stylesheet" href="/theme/css/voidybootstrap.css" />

    <link rel="shortcut icon" href="/favicon.ico" />
  </head>

  <body>
   
    <nav class="navbar navbar-default">
      <div class="container">
	   <div class="navbar-header">
		<button type="button" class="navbar-toggle" 
				data-toggle="collapse" data-target="#main-navbar-collapse">
		  <span class="sr-only">Toggle navigation</span>
		  <span class="icon-bar"></span>
		  <span class="icon-bar"></span>
		  <span class="icon-bar"></span>
		</button>
		<a class="navbar-brand" href="/" rel="home">
          <i class="fas fa-home fa-fw fa-lg"> </i> </a>
       </div>

      <div class="collapse navbar-collapse" id="main-navbar-collapse">
        <ul class="nav navbar-nav">
              <li>
                <a href="/pages/about-this-blog.html">About this Blog</a>
              </li>
            <li class="divider"></li>
            <li class="">
              <a href="/archives.html">Archives</a>
            </li>
          <li class="divider"></li>
        </ul> <!-- /nav -->
      </div> <!-- /navbar-collapse -->
	  </div> <!-- /container -->
    </nav> <!-- /navbar -->

	<div class="jumbotron" id="overview">
	  <div class="container">
		<h1><a href="/">fedops blog</a></h1>
		<p class="lead">Privacy in Computing</p>
	  </div>
	</div>

    <div class="container" id="main-container">
      <div class="row">
        <div class="col-md-9" id="content">
<article itemscope="itemscope" itemtype="http://schema.org/BlogPosting">
  <header class="article-header">
<abbr class="article-header-date">
  Mon 06 March 2023
</abbr> <h1>
  <a href="/my-foss-photo-workflow.html" rel="bookmark"
     title="Permalink to My FOSS Photo Workflow">
    My FOSS Photo Workflow
  </a>
</h1><div class="article-header-info">
  <p>
      Posted by <a href="/author/fedops.html">fedops</a>
    in 
    <a href="/category/software.html">
      Software</a>
    &nbsp;&nbsp;
  </p>
</div> <!-- /.article-header-info -->  </header>
  <div class="content-body" itemprop="text articleBody">
	<p>Digital photography workflows tend to be dominated by a relatively small number
of proprietary, commercial software suites that have been optimized over years
or even decades. Adobe Lightroom, Capture One, or Affinity Photo are household
names, and they're all available for both MacOS as well as Windows. Which also
means that most prosumer and professional photographers use those operating
systems as the fine-tuned applications are their bread-and-butter tools they
generally can't do without.</p>
<p>A small number of open source enthusiasts develop and use some remarkable, but
largely unremarked, programs that aim to replace those walled gardens and also
work on free operating systems such as Linux. A very good site
to find information is <a href="https://pixls.us/">pixls.us</a>.</p>
<div class="toc"><span class="toctitle">Table of Contents</span><ul>
<li><a href="#copying-images-off-the-camera">Copying Images off the Camera</a></li>
<li><a href="#photo-organization">Photo Organization</a></li>
<li><a href="#culling-the-herd">Culling the Herd</a></li>
<li><a href="#assigning-metadata">Assigning Metadata</a></li>
<li><a href="#editing">Editing</a></li>
<li><a href="#postprocessing-and-resizing">Postprocessing and Resizing</a></li>
<li><a href="#alternatives">Alternatives</a></li>
</ul>
</div>
<p>In the following paragraphs I'll outline my own personal workflow which might
provide some useful pointers if you're just starting out. I'm shooting using a
Canon DSLR and use Fedora Linux, but I believe most things should work
regardless of camera manufacturer and distro choice.</p>
<h1 id="copying-images-off-the-camera">Copying Images off the Camera</h1>
<p>The first step in the workflow is getting the pixels out of the camera. One
option is to pop the memory card into a card reader and transfer them, e.g.
using <a href="https://damonlynch.net/rapid/index.html">rapid photo downloader</a>.</p>
<p>I don't have a Compact Flash reader so I prefer to connect the camera to my PC
with a USB cable. I then use <a href="http://www.gphoto.org/doc/">gphoto2</a> to obtain the
data, which is the swiss army-knife of cli camera utilities. It is also
wonderfully scriptable. Following are a few useful commands:</p>
<pre><code># show automatically detected cameras
gphoto2 --auto-detect
# summary info
gphoto2 --summary
# show number of files
gphoto2 -n
gphoto2 -n -f /store_00010001/DCIM/100EOS7D
# list folders
gphoto2 -l
# list all files
gphoto2 -L
# copy file range (by numbers)
gphoto2 --get-file 7-12
# only copy files newer than what's in the current dir
gphoto2 --new
</code></pre>
<p>Generally I use the <code>gphotofs</code> package to mount the camera's filesystem
directly:</p>
<pre><code># mount camera
gphotofs /mnt/eos
# umount
fusermount -u /mnt/eos
</code></pre>
<p>This makes it possible to manipulate the in-camera image files using the usual
commands such as <code>cp</code> and <code>ls</code>, or indeed using a file manager. Some examples:</p>
<pre><code># copy files not older than 7 days
cp --preserve=all `find /mnt/eos/store_00010001/DCIM/100EOS7D/ -mtime -7 -print` .
# copy files created between 2021-05-08 and 2021-05-10
cp --preserve=all `find /mnt/eos/store_00010001/DCIM/100EOS7D/ -type f \
    -newermt 2021-05-08 ! -newermt 2021-05-10 -print` .
# sync all files
rsync -av /mnt/eos/store_00010001/DCIM/100EOS7D/ .
</code></pre>
<p>These commands are quite useful. The rsync command should only be used if you
keep all your image files in one directory, which is not a great way if you
shoot a lot. This brings me to the next chapter and a very fundamental decision:</p>
<h1 id="photo-organization">Photo Organization</h1>
<p>I recommend to define a scheme of how you manage your image files, and
then stick to it as strictly as possible. Here's mine:</p>
<ul>
<li>I have one directory which holds all my images, and which I always synchronize
  and backup in its entirety.</li>
<li>for each trip/outing I create a directory inside of this directory, giving it a
  descriptive name. My format is something like <code>spain-2023-01</code> or
  <code>nature-2023-02</code>. If I have a trip log or some other documentation of the shoot such as model
  release forms those will also be stored here. I call this the "shoot
  directory".</li>
<li>for extended trips I will create further subdirectories, usually 
  one per day, such as <code>day01</code>, <code>day02</code>, ... This is especially necessary if
  there's a danger that the camera-internal image numbering will wrap around and
  thus image names may not be unique anymore. Each directory holds the RAW files
  from the camera from that day.</li>
<li>in the shoot directory there is a <code>edits</code> subdirectory into which I store all the
  edited files such as JPG exports.</li>
<li>I leave my file names as they come out of the camera, but you may chose to
  rename them to whatever scheme you would like. Rapid photo downloader and most
  other programs support on-the-fly bulk renaming.</li>
</ul>
<p>Whatever scheme you use, make sure everything's settled in before proceeding to
the next step, which is:</p>
<h1 id="culling-the-herd">Culling the Herd</h1>
<p>If you're like me, you will not want to keep all the images you brought back
from a trip. In sport or wildlife photography for example usually up to 90% of the
images are somehow defective and need to be deleted to maintain an overview of
what's there and identify the "keepers".</p>
<p>My preferred way is to import the entire shoot directory into
<a href="https://www.digikam.org/">digikam</a> using <code>Import --&gt; Add Images...</code>. Digikam
will then start to create thumbnails, it's best to give it some time to finish
this process after importing. The user interface is somewhat unusual and
definitely doesn't have the refined polish of Lightroom, but invest some time to
familiarize yourself and it is suprisingly capable.</p>
<p>Next, go through all the images one by one, compare them, and delete undesirable
ones. The easiest way is to mark images as "rejected" (red flag, which I have
bound to the "x" key) and then later delete them all at once. The main objective
of this step is to get rid of the cruft that won't be worked on anyway.</p>
<p>Now is also a good time to assign ratings (using the number keys 0-5) to make it
easier to find the images I will process in the next step. Those images (I call
them "keepers") get a rating of 3 stars. I usually don't rate others. My camera
supports rating via a special button, which I will sometimes use when I got a
shot I can already see is great even on the camera display. Digikam picks this
rating up from the EXIF tag and you'll see it in this step.</p>
<p>Once I have gone through all images of the shoot I filter for "rejected" only,
mark them all, and then delete them.</p>
<h1 id="assigning-metadata">Assigning Metadata</h1>
<p>Ensuring proper metadata makes filtering and later retrieval easier. I only orry
about metadata in my "3 stars or better" images so the first step is to turn on
the filter to see just them.</p>
<p>I have a fairly elaborate tree structure of tags, of which I assign the suitable
ones in this step. For example I like to photograph airplanes, so a typical tag
structure might look like:</p>
<ul>
<li>Boeing<ul>
<li>B737</li>
<li>B777<ul>
<li>B777-200</li>
<li>B777-300ER</li>
</ul>
</li>
</ul>
</li>
<li>Airbus<ul>
<li>A340</li>
</ul>
</li>
</ul>
<p>Assigning "B777-200" will also assign the "B777" and "Boeing" tags, which makes
it easy to filter and search for those more generic terms later on.</p>
<p>The most time-consuming steps are to set the titles and captions. In my world
the title describes the location and date of the image. So for example
"Kruger Park, April 2020". Chances are it's the same for many or all of the
images, so I select all required images and set the title at once.</p>
<p>The caption contains a description of the image, maybe "Lilac-breasted roller
(Coracias caudatus) in a tree". Setting these is a bit of work, but this is
searchable data so will come in quite handily later on when you need to find all
pictures of rollers in your collection.</p>
<p>My camera has a GPS module so the geolocation data will be inside the RAW
images. If you apply this data manually you may want to do it at this time.</p>
<p>This completes the library step if you're used to Lightroom. It is important to
note that every image which should be processed later must be "touched" at this time.
Modifying metadata means a sidecar file is created, which we need in the next
step.</p>
<h1 id="editing">Editing</h1>
<p>I do my editing in ART, so called because it is <a href="https://bitbucket.org/agriggio/art/wiki/Home">A fork of
RawTherapee</a>. There are many
significant differences, such as a reordering of the tools to form a different
and somewhat simplified workflow within the application. It also understands XMP
sidecar files, which RawTherapee unfortunately does not, and which are key to
this workflow. ART is available in multiple formats, including universally
useful Appimages, which I use.</p>
<p>Start ART and load the directory to be worked on. This adds all the images to
the current view. I then set the view filter to only show images rated 3 stars or
more.</p>
<p>I do not edit my images heavily. In general my steps are:</p>
<ol>
<li>enter edit mode and load "resize1920x1600" profile for presets</li>
<li>rotate to straighten lines, optionally correct perspective</li>
<li>adjust exposure and contrast (lightly, if required)</li>
<li>adjust highlights and shadows</li>
<li>turn on post-resize sharpening</li>
<li>update metadata (min. Description and Title, should be auto-filled from
   sidecar file)</li>
<li>add to queue (Ctrl+b)</li>
</ol>
<p>The "resize1920x1600" profile sets the output dimensions accordingly and turns
on output sharpening. It needs to be defined once in the profile settings.</p>
<p>A nice quality of life improvement is that Control+C and Control+V will copy and
paste the editing steps from one file to the next. So if you have multiple
consecutive images they can be modified quickly and consistently.</p>
<p>Sometimes during editing I will discover that an image isn't as good as I
thought. I will then down-rate it to 2 or even 1, which removes it from the
current filtered view. If I need to find a better alternative from a set of
similar images I disable the filter, find a substitute, rate it as 3 stars, and
re-enable the filter.</p>
<p>Another nice feature are the color labels. Generally I label pictures as "green"
when I am done working on them, and ones I need to revisit later as "red".
This comes in very handy during lengthy editing sessions. If I break for the
night and come back the next day I can see what has already been done
and what I still need to work on.</p>
<p>Once I'm happy with the edits I run the queue which I have configured to export
the files as 1920x1600 TIFFs into a subdirectory "edits" in the current
directory (which will be created if required). If you have a fast machine you
can set the queue to "auto run" which means any new entries will get processed
in the background. On slower machines it might be better to run the queue when
you don't need the horsepower for editing.</p>
<h1 id="postprocessing-and-resizing">Postprocessing and Resizing</h1>
<p>Two things remain to be done. I want to convert the TIFFs to JPGs with an
overlaid watermark, and I want to pull in the metadata that was added so
laboriously in digikam to the JPG. This is handled by two shell scripts working
together. These require the <code>exiftools</code> and <code>ImageMagick</code> packages to be
installed on your system.</p>
<p>First, I cd into the "edits" directory where ART dumped the TIFFs. Then this
script will loop over all TIFFS and determine whether a JPG exists and isn't
newer than the parent TIFF. It will then run the second script <code>add-wm.sh</code> on
that file:</p>
<pre><code>#!/bin/bash
#
for i in *.tif
do  base=`echo $i | sed 's/.tif//'`
    if [ &quot;${base}-wm.jpg&quot; -ot $i ]
    then    add-wm.sh $i
    fi
done
</code></pre>
<p>This second script is a bit more involved:</p>
<pre><code>#!/bin/bash
# add watermark to image and convert to JPG

# &quot;${1%.*}&quot; is just the part of the filename up to the first '.'
echo -n ${1} + wm --\&gt; &quot;${1%.*}&quot;-wm.jpg

# overlay watermark image and save as JPG
#composite -quiet -dissolve 50% -gravity southeast ~/.config/misc/watermark2020.png ${1} &quot;${1%.*}&quot;-wm.jpg
composite -quiet -dissolve 50% -gravity southeast ~/.config/misc/1x1.png ${1} &quot;${1%.*}&quot;-wm.jpg

# copy exiftags from TIF into newly created JPG
exiftool -tagsFromFile ${1} &quot;${1%.*}&quot;-wm.jpg &gt;/dev/null 2&gt;&amp;1

# pull in tags from XMP sidecar file if it exists
# (if this TIF was generated by Lightroom it's missing those tags)
if [ -e &quot;../${1%.*}&quot;.CR2.xmp ]
then    echo -n &quot; ...with tags from ../${1%.*}&quot;.CR2.xmp
        exiftool -tagsFromFile &quot;../${1%.*}&quot;.CR2.xmp -all:all &quot;${1%.*}&quot;-wm.jpg &gt;/dev/null 2&gt;&amp;1
fi
echo &quot;&quot;

# copy Caption-Abstract to Description field (workaround for ART)
exiftool '-Description&lt;Caption-Abstract' &quot;${1%.*}&quot;-wm.jpg &gt;/dev/null 2&gt;&amp;1

# get rid of the &quot;_original&quot; file
rm -f &quot;${1%.*}&quot;-wm.jpg_original
</code></pre>
<p>Some explanations:</p>
<ul>
<li>the two "composite" lines either add a watermark with my full name and logo or
  just a transparent pixel and create the JPG file. I comment out what I don't
  want.</li>
<li>the first exiftool command then transfers the tags from the TIFF to the JPG.</li>
<li>the if-block looks for an XMP sidecar file and if found will also add those
  tags to the JPG file.</li>
<li>the final exiftool command copies the Caption/Abstract field (which ART
  populates with the caption) to the IPCC Description field (which most software
  expects the caption to live in).</li>
</ul>
<p>At this point I have the JPGs and will look through them to see what they look
like. Sometimes a bit more editing may be required which just repeats the above
steps in ART and then a rerun of the <code>watermarks</code> script rebuilds the JPGs of
the changed files only.</p>
<h1 id="alternatives">Alternatives</h1>
<p>This is just my somewhat specialized workflow. I'd be the first to agree it is
quite involved but it works for me and doesnt shoehorn me into any proprietary
software or huge binary image databases. Everything remains in individual files
and there is no questionable exporting required should I decide to change software.</p>
<p>I have previously experimented with other software. One which I would be remiss
in not mentioning is <a href="https://www.darktable.org/">Darktable</a>. It is an extremely
capable all-in-one database management and editing application closely
patterned after Lightroom. Where Lightroom is limited by Adobe's protectionism
towards their flagship (and much more expensive) Photoshop suite, Darktable's
authors pull out all the stops and thus the program has far surpassed the
original's functionality.</p>
<p>Personally I was somewhat overwhelmed by it and found I got better results with
ART but I still suggest Darktable is worth a try. If you work with photos
regularly, possibly daily, it might be just the ticket. YMMV.</p>
  </div>
  
<div class="article-tag-list">
<span class="label label-default">Tags</span>
	<a href="/tag/linux.html"><i class="fas fa-tag"></i>linux</a>&nbsp;
	<a href="/tag/software.html"><i class="fas fa-tag"></i>software</a>&nbsp;
	<a href="/tag/photography.html"><i class="fas fa-tag"></i>photography</a>&nbsp;
</div><!-- via neighbor plugin, see: https://github.com/pelican-plugins/neighbors -->
    <hr />
    <p class="content-emphasis">
	<table width="100%">
	    <tr>
			<td align="left">
	        </td>
	        <td align="right">
	        </td>
		</tr>
	</table>
	</p>
</article>
        </div><!-- /content -->

        <div class="col-md-3 sidebar-nav" id="sidebar">

<div class="row">

<div class="col-xs-6 col-md-12">
<h4><i class="fas fa-comment fa-fw fa-lg"></i> Social</h4>
<ul class="list-unstyled social-links">
    <li><a href="https://fosstodon.org/@fedops" target="_blank">
	  <i class="fas fa-comments fa-fw fa-lg" title="Fosstodon"></i>
		Fosstodon
	</a></li>
    <li><a href="https://codeberg.org/fedops" target="_blank">
	  <i class="fas fa-comments fa-fw fa-lg" title="Codeberg"></i>
		Codeberg
	</a></li>
</ul>
</div>

<div class="col-xs-6 col-md-12">
<h4><i class="fas fa-folder fa-fw fa-lg"></i> Categories</h4>
<ul class="list-unstyled category-links">
  <li><a href="/category/cloud.html" >
    <i class="fas fa-folder-open fa-fw fa-lg"></i> Cloud</a></li>
  <li><a href="/category/hardware.html" >
    <i class="fas fa-folder-open fa-fw fa-lg"></i> Hardware</a></li>
  <li><a href="/category/howto.html" >
    <i class="fas fa-folder-open fa-fw fa-lg"></i> Howto</a></li>
  <li><a href="/category/infomanagement.html" >
    <i class="fas fa-folder-open fa-fw fa-lg"></i> InfoManagement</a></li>
  <li><a href="/category/media.html" >
    <i class="fas fa-folder-open fa-fw fa-lg"></i> Media</a></li>
  <li><a href="/category/misc.html" >
    <i class="fas fa-folder-open fa-fw fa-lg"></i> misc</a></li>
  <li><a href="/category/phone.html" >
    <i class="fas fa-folder-open fa-fw fa-lg"></i> Phone</a></li>
  <li><a href="/category/privacy.html" >
    <i class="fas fa-folder-open fa-fw fa-lg"></i> Privacy</a></li>
  <li><a href="/category/security.html" >
    <i class="fas fa-folder-open fa-fw fa-lg"></i> Security</a></li>
  <li><a href="/category/software.html" >
    <i class="fas fa-folder-open fa-fw fa-lg"></i> Software</a></li>
</ul>
</div>

</div> <!-- /row -->

  <h4><i class="fas fa-link fa-fw fa-lg"></i> Links</h4>
  <ul class="list-unstyled category-links">
    <li><a href="https://getpelican.com/" >
      <i class="fas fa-external-link-square-alt fa-fw fa-lg"></i> Pelican</a></li>
    <li><a href="https://www.python.org/" >
      <i class="fas fa-external-link-square-alt fa-fw fa-lg"></i> Python.org</a></li>
    <li><a href="https://palletsprojects.com/p/jinja/" >
      <i class="fas fa-external-link-square-alt fa-fw fa-lg"></i> Jinja2</a></li>
  </ul>
<h4><i class="fas fa-tags fa-fw fa-lg"></i> Tags</h4>
<p class="tag-cloud">
      <a href="/tag/linux.html">
        <i class="fas fa-tag fa-fw fa-lg"></i>linux
      </a>
      <a href="/tag/os.html">
        <i class="fas fa-tag fa-fw fa-lg"></i>os
      </a>
      <a href="/tag/fedora.html">
        <i class="fas fa-tag fa-fw fa-lg"></i>fedora
      </a>
      <a href="/tag/networking.html">
        <i class="fas fa-tag fa-fw fa-lg"></i>networking
      </a>
      <a href="/tag/wireguard.html">
        <i class="fas fa-tag fa-fw fa-lg"></i>wireguard
      </a>
      <a href="/tag/privacy.html">
        <i class="fas fa-tag fa-fw fa-lg"></i>privacy
      </a>
      <a href="/tag/media.html">
        <i class="fas fa-tag fa-fw fa-lg"></i>media
      </a>
      <a href="/tag/hardware.html">
        <i class="fas fa-tag fa-fw fa-lg"></i>hardware
      </a>
      <a href="/tag/software.html">
        <i class="fas fa-tag fa-fw fa-lg"></i>software
      </a>
      <a href="/tag/smarthome.html">
        <i class="fas fa-tag fa-fw fa-lg"></i>smarthome
      </a>
      <a href="/tag/sustainability.html">
        <i class="fas fa-tag fa-fw fa-lg"></i>sustainability
      </a>
      <a href="/tag/mqtt.html">
        <i class="fas fa-tag fa-fw fa-lg"></i>mqtt
      </a>
      <a href="/tag/photography.html">
        <i class="fas fa-tag fa-fw fa-lg"></i>photography
      </a>
      <a href="/tag/security.html">
        <i class="fas fa-tag fa-fw fa-lg"></i>security
      </a>
      <a href="/tag/mobile.html">
        <i class="fas fa-tag fa-fw fa-lg"></i>mobile
      </a>
      <a href="/tag/quicktip.html">
        <i class="fas fa-tag fa-fw fa-lg"></i>quicktip
      </a>
      <a href="/tag/sun.html">
        <i class="fas fa-tag fa-fw fa-lg"></i>sun
      </a>
      <a href="/tag/arduino.html">
        <i class="fas fa-tag fa-fw fa-lg"></i>arduino
      </a>
      <a href="/tag/web.html">
        <i class="fas fa-tag fa-fw fa-lg"></i>web
      </a>
      <a href="/tag/bookmarks.html">
        <i class="fas fa-tag fa-fw fa-lg"></i>bookmarks
      </a>
      <a href="/tag/documentation.html">
        <i class="fas fa-tag fa-fw fa-lg"></i>documentation
      </a>
      <a href="/tag/blog.html">
        <i class="fas fa-tag fa-fw fa-lg"></i>blog
      </a>
      <a href="/tag/housekeeping.html">
        <i class="fas fa-tag fa-fw fa-lg"></i>housekeeping
      </a>
      <a href="/tag/markdown.html">
        <i class="fas fa-tag fa-fw fa-lg"></i>markdown
      </a>
      <a href="/tag/cloud.html">
        <i class="fas fa-tag fa-fw fa-lg"></i>cloud
      </a>
      <a href="/tag/spyware.html">
        <i class="fas fa-tag fa-fw fa-lg"></i>spyware
      </a>
      <a href="/tag/phone.html">
        <i class="fas fa-tag fa-fw fa-lg"></i>phone
      </a>
      <a href="/tag/applications.html">
        <i class="fas fa-tag fa-fw fa-lg"></i>applications
      </a>
      <a href="/tag/chat.html">
        <i class="fas fa-tag fa-fw fa-lg"></i>chat
      </a>
      <a href="/tag/mastodon.html">
        <i class="fas fa-tag fa-fw fa-lg"></i>mastodon
      </a>
      <a href="/tag/gui.html">
        <i class="fas fa-tag fa-fw fa-lg"></i>gui
      </a>
      <a href="/tag/application.html">
        <i class="fas fa-tag fa-fw fa-lg"></i>application
      </a>
</p>

<hr />

        </div><!--/sidebar -->
      </div><!--/row-->
    </div><!--/.container /#main-container -->

    <footer id="site-footer">
 
      <address id="site-colophon">
        <p class="text-center text-muted">
        Site built using <a href="http://getpelican.com/" target="_blank">Pelican</a>
        &nbsp;&bull;&nbsp; Theme based on
        <a href="http://www.voidynullness.net/page/voidy-bootstrap-pelican-theme/"
           target="_blank">VoidyBootstrap</a> by 
        <a href="http://voidynullness.net"
           target="_blank">RKI</a>  
        </p>
      </address><!-- /colophon  -->
    </footer>


    <!-- javascript -->
   
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js" integrity="sha384-nvAa0+6Qg9clwYCGGPpDQLVpLNn0fRaROjHqs13t4Ggj3Ez50XnGQqc/r8MhnRDZ" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
            integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
            crossorigin="anonymous"></script>


  </body>
</html>